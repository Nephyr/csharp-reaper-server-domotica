	
	block_selection = function(target)
	{
		if (typeof target.onselectstart!="undefined") target.onselectstart = function(){
				return false
			}
		else if (typeof target.style.MozUserSelect!="undefined") target.style.MozUserSelect = "none"
		else target.onmousedown = function(){
				return false 
			}
		target.style.cursor = "default"
	}
	
	searchby = function(e)
	{
		var handle_doc = document.getElementById("search_bar_1");
		var handle_res = document.getElementById("result_1");
		if(trim(handle_doc.value, " ") != "")
		{
			handle_res.innerHTML = "";
			
			for(i=0; i<index.length; i++)
			{
				if(index[i].indexOf(trim(handle_doc.value, " ").toLowerCase()) != -1)
				{
					if(isHelp)
					{
						handle_res.innerHTML += "<a href='" + index[i] + ".htm'>" + index[i] + "</a><br />";
					}
					else
					{
						handle_res.innerHTML += "<a href='help/" + index[i] + ".htm'>" + index[i] + "</a><br />";
					}
				}
			}
		}
		else
		{
			if(isHelp) location.href="../default.htm";
			else location.href="default.htm";
		}
		return false;
	}
	
	//-------------------------------------------------------------------------------------| TRIM - FUNCTION
	
	trim = function(string, chars) {
		return ltrim(rtrim(string, chars), chars);
	}
	 
	ltrim = function(string, chars) {
		chars = chars || "\\s";
		return string.replace(new RegExp("^[" + chars + "]+", "g"), "");
	}
	 
	rtrim = function(string, chars) {
		chars = chars || "\\s";
		return string.replace(new RegExp("[" + chars + "]+$", "g"), "");
	}
	
	//-------------------------------------------------------------------------------------| TRIM - FUNCTION
		
	window.onload = function()
	{
		block_selection(document.body);
	}