﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Reaper_Monitor
{
    public partial class Form1 : Form
    {
        private System.ServiceProcess.ServiceController pReaperController   = new System.ServiceProcess.ServiceController();
        private RemoteService _RemoteConn                                   = new RemoteService();

        System.Threading.Thread _s                                          = null;
        System.Threading.Thread _e                                          = null;

        public bool close_enable                                            = false;
        public bool notif_enable                                            = true;

        //===========================================================================================

        public Form1()
        {
            InitializeComponent();
            //---->
            if (!IsServiceInstalled("Reaper - TCP Server"))
            {
                this.Close();
            }

            pReaperController = new System.ServiceProcess.ServiceController("Reaper - TCP Server");

            //
            //  CHECK SERVICE
            //
            _s = new System.Threading.Thread(this.ServiceChecker);
            _e = new System.Threading.Thread(this.EvenetLogUpdater);
            _s.Start();
            _e.Start();

            //
            //  INIT. HELP SERVICE
            //
            this.webBrowser1.Navigate("file://" + System.Environment.CurrentDirectory + "/help/default.htm");

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            listView1.Columns[0].Width = (int)160;
            listView1.Columns[1].Width = (int)listView1.Width - 430;
            listView1.Columns[2].Width = (int)120;
            listView1.Columns[3].Width = (int)120;
            listView1.Invalidate();

            richTextBox1.Select(richTextBox1.TextLength, 0);

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            _s.Abort();
            _e.Abort();

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!close_enable)
            {
                e.Cancel = true;
                this.WindowState = FormWindowState.Minimized;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ShowInTaskbar = false;
                this.Visible = false;
                if (notif_enable)
                {
                    notifyIcon1.Visible = true;
                    notifyIcon1.ShowBalloonTip(30000, "Attenzione!", "Il Monitor continuerà ad essere in esecuzione ridotto a icona." +
                        " Per chiuderlo cliccare su 'Termina Attività' cliccando con tasto destro sull'icona o mediante menù File.", ToolTipIcon.Info);
                    notif_enable = false;
                }
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            close_enable = true;
            this.Close();

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        private void fToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.Sizable;
            WindowState = FormWindowState.Maximized;
        }

        private void toggleFullScreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized && FormBorderStyle == FormBorderStyle.None)
            {
                WindowState = FormWindowState.Normal;
                FormBorderStyle = FormBorderStyle.Sizable;
                WindowState = FormWindowState.Maximized;
            }
            else
            {
                WindowState = FormWindowState.Normal;
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;
            }
        }

        private void minimizedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.Sizable;
            WindowState = FormWindowState.Minimized;
        }
    }
}
