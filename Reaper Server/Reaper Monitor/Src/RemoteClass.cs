﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper_Monitor
{
    public partial class RemoteService
    {
        private System.Net.Sockets.TcpClient pLocalSystemConn               = null;

        //===========================================================================================

        public RemoteService()
        {
        }

        //===========================================================================================

        public bool Connect()
        {
            byte[] buffer = new byte[21];

            pLocalSystemConn = new System.Net.Sockets.TcpClient();
            pLocalSystemConn.Connect(System.Net.IPAddress.Parse("127.0.0.1"), 3726);

            pLocalSystemConn.Client.Send(System.Text.Encoding.ASCII.GetBytes("SMSG_SOCKET_CONN_START[FINAL]"));
            pLocalSystemConn.Client.Receive(buffer);

            if (System.Text.Encoding.ASCII.GetString(buffer).Substring(0, 19) == "SMSG_SOCKET_CONN_OK")
            {
                pLocalSystemConn.Client.Send(System.Text.Encoding.ASCII.GetBytes("SMSG_LOGIN_ACCOUNT 17c40a1075c6c2b2248a8a8d51d0686d5a56b45e[FINAL]"));
                pLocalSystemConn.Client.Receive(buffer);

                if (System.Text.Encoding.ASCII.GetString(buffer) == "SMSG_LOGIN_ACCOUNT_OK")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        //===========================================================================================

        public void Disconnect()
        {
            pLocalSystemConn.Client.Send(System.Text.Encoding.ASCII.GetBytes("SMSG_SOCKET_CONN_FINISH[FINAL]"));
            pLocalSystemConn.Client.Disconnect(false);
            pLocalSystemConn = null;
        }

        //===========================================================================================

        public System.Net.Sockets.Socket GetClientNet()
        {
            return pLocalSystemConn.Client;
        }

        //===========================================================================================
        //
        //      DICHIARAZIONE FUNZIONE: INVIO PACCHETTI
        //
        public int SendPackage(string cBuffer, int timeout)
        {
            int startTickCount = System.Environment.TickCount;
            int sent = 0;
            int offset = 0;
            this.pLocalSystemConn.Client.SendTimeout = timeout;
            //---->
            System.Collections.Generic.List<byte[]> buffer = new System.Collections.Generic.List<byte[]>();
            buffer.Add(System.Text.Encoding.ASCII.GetBytes(cBuffer));
            //---->
            do
            {
                try
                {
                    System.Threading.Thread.Sleep(1); // FIX - CARICO CPU (Riduzione del carico del 75%)
                    sent += this.pLocalSystemConn.Client.Send(buffer[0], offset + sent, buffer[0].Length - sent,
                        System.Net.Sockets.SocketFlags.None);
                }
                catch (System.Net.Sockets.SocketException ex)
                {
                    if (ex.SocketErrorCode == System.Net.Sockets.SocketError.WouldBlock ||
                        ex.SocketErrorCode == System.Net.Sockets.SocketError.IOPending ||
                        ex.SocketErrorCode == System.Net.Sockets.SocketError.NoBufferSpaceAvailable)
                    {
                        System.Threading.Thread.Sleep(1); // FIX - CARICO CPU (Riduzione del carico del 75%)
                    }
                    else
                    {
                        break;
                    }
                }
            } while (sent < buffer[0].Length && !System.Text.Encoding.ASCII.GetString(buffer[0]).Contains("[FINAL]"));

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

            return buffer[0].Length;
        }

        //===========================================================================================
        //
        //      DICHIARAZIONE FUNZIONE: RICEZIONE PACCHETTI
        //
        public string ReceivePackage(int bytes, int timeout)
        {
            int received = 0;
            int offset = 0;
            this.pLocalSystemConn.Client.ReceiveTimeout = timeout;
            byte[] buffer = new byte[bytes];
            string temp = string.Empty;
            //---->
            try
            {
                do
                {
                    System.Threading.Thread.Sleep(1); // FIX - CARICO CPU (Riduzione del carico del 75%)
                    //---->
                    received += this.pLocalSystemConn.Client.Receive(buffer, offset + received, buffer.Length - received,
                        System.Net.Sockets.SocketFlags.None);
                    //---->
                    temp += System.Text.Encoding.ASCII.GetString(buffer);

                } while (received < buffer.Length && !System.Text.Encoding.ASCII.GetString(buffer).Contains("[FINAL]"));
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                if (ex.SocketErrorCode != System.Net.Sockets.SocketError.WouldBlock ||
                    ex.SocketErrorCode != System.Net.Sockets.SocketError.IOPending ||
                    ex.SocketErrorCode != System.Net.Sockets.SocketError.NoBufferSpaceAvailable)
                {
                    return ex.Message;
                }
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

            return temp.Replace("[FINAL]", "").Trim();
        }
    }
}
