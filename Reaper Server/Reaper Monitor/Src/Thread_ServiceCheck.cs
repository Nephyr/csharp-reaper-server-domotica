﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ServiceProcess;

namespace Reaper_Monitor
{
    public partial class Form1
    {
        public static bool IsServiceInstalled(string serviceName)
        {
            ServiceController[] services = ServiceController.GetServices();
            foreach (ServiceController service in services)
            {
                if (service.DisplayName == serviceName)
                    return true;
            }
            return false;
        }

        public void ServiceChecker()
        {
            try
            {
                while (System.Threading.Thread.CurrentThread.ThreadState == System.Threading.ThreadState.Running && pReaperController != null)
                {
                    System.Threading.Thread.Sleep(1500); // FIX Carico CPU
                    //---->
                    this.pReaperController.Refresh();
                    switch (this.pReaperController.Status)
                    {
                        //
                        //  STATUS - RUNNING
                        //
                        case System.ServiceProcess.ServiceControllerStatus.Running:
                            this.notifyIcon1.Icon = new System.Drawing.Icon(System.Environment.CurrentDirectory + "/res/green-on.ico");
                            avviaserverToolStripMenuItem.Enabled = false;
                            avviaToolStripMenuItem.Enabled = false;
                            fermaServerToolStripMenuItem.Enabled = true;
                            fermaToolStripMenuItem.Enabled = true;
                            toolStripStatusLabel2.Text = "Status: Running";
                            break;

                        //
                        //  STATUS - PAUSED
                        //
                        case System.ServiceProcess.ServiceControllerStatus.Paused:
                            this.notifyIcon1.Icon = new System.Drawing.Icon(System.Environment.CurrentDirectory + "/res/yellow-on.ico");
                            toolStripStatusLabel2.Text = "Status: Paused";
                            break;

                        //
                        //  STATUS - STANDBY
                        //
                        case System.ServiceProcess.ServiceControllerStatus.StopPending:
                        case System.ServiceProcess.ServiceControllerStatus.StartPending:
                        case System.ServiceProcess.ServiceControllerStatus.PausePending:
                        case System.ServiceProcess.ServiceControllerStatus.ContinuePending:
                            this.notifyIcon1.Icon = new System.Drawing.Icon(System.Environment.CurrentDirectory + "/res/white-on.ico");
                            toolStripStatusLabel2.Text = "Applying...";
                            break;

                        //
                        //  STATUS - STOPPED
                        //
                        case System.ServiceProcess.ServiceControllerStatus.Stopped:
                        default:
                            this.notifyIcon1.Icon = new System.Drawing.Icon(System.Environment.CurrentDirectory + "/res/red-on.ico");
                            avviaserverToolStripMenuItem.Enabled = true;
                            avviaToolStripMenuItem.Enabled = true;
                            fermaServerToolStripMenuItem.Enabled = false;
                            fermaToolStripMenuItem.Enabled = false;
                            toolStripStatusLabel2.Text = "Status: Stopped";
                            break;
                    }

                    // Garbage Collection
                    System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
                }
            }
            catch (System.Exception)
            {
                Close();
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }
    }
}
