﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Reaper_Monitor
{
    public partial class Form1
    {
        //
        //  BTN ---> MOUSE_DX.APRI_MONITOR
        //
        private void apriMonitorDiServizioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Visible = true;
            this.ShowInTaskbar = true;
            this.WindowState =  FormWindowState.Normal;
            this.CenterToScreen();

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //
        //  BTN ---> MOUSE_DX.CHIUDI_MONITOR
        //
        private void terminaAttivitàToolStripMenuItem_Click(object sender, EventArgs e)
        {
            close_enable = true;
            this.Close();

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }
    }
}
