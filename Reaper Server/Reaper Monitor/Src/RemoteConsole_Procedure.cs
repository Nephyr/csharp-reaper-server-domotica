﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Reaper_Monitor
{
    public partial class Form1
    {
        private string cache_char = "";
        private bool console_input_enable = true;

        private void richTextBox1_Enter(object sender, EventArgs e)
        {
            richTextBox1.Select(richTextBox1.Text.Length, 0);

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        private void richTextBox1_Click(object sender, EventArgs e)
        {
            richTextBox1.Select(richTextBox1.Text.Length, 0);

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //
        //  CONSOLE REMOTA
        //
        private void richTextBox1_KeyPress(object sender, KeyEventArgs e)
        {
            KeysConverter key = new KeysConverter();
            e.SuppressKeyPress = false;

            if (!console_input_enable)
            {
                e.SuppressKeyPress = true;
                return;
            }

            if (e.KeyCode == Keys.Right ||
                e.KeyCode == Keys.Up ||
                e.KeyCode == Keys.Down ||
                e.KeyCode == Keys.Left ||
                e.KeyCode == Keys.PageDown ||
                e.KeyCode == Keys.PageUp ||
                e.KeyCode == Keys.End ||
                e.KeyCode == Keys.Home ||
                e.KeyCode == Keys.Shift ||
                e.KeyCode == Keys.ControlKey ||
                e.KeyCode == Keys.ShiftKey ||
                e.KeyCode == Keys.Control ||
                e.KeyCode == Keys.Tab ||
                e.KeyCode == Keys.Menu ||
                e.KeyCode == Keys.Modifiers)
            {
                e.SuppressKeyPress = true;
                return;
            }

            if (e.KeyCode == Keys.Return)
            {
                if (cache_char != string.Empty)
                {
                    console_input_enable = false;
                    ExecuteCodeByCommand(cache_char);
                    console_input_enable = true;
                    richTextBox1.AppendText("\n" + @"root@reaper:\> ");
                    richTextBox1.Focus();
                }
                e.SuppressKeyPress = true;

                // auto-scroll
                richTextBox1.Select(richTextBox1.TextLength, 0);

                // Clear della cache
                cache_char = string.Empty;
            }
            else if (e.KeyCode == Keys.Back)
            {
                if (cache_char.Length > 0)
                {
                    cache_char = cache_char.Substring(0, cache_char.Length - 1);
                }
                else
                {
                    e.SuppressKeyPress = true;

                    // Clear della cache
                    cache_char = string.Empty;
                }
            }
            else
            {
                cache_char += key.ConvertToString(e.KeyCode).ToLower();
                cache_char = cache_char.Replace("oemperiod", ".");
                cache_char = cache_char.Replace("oemcomma", ",");
                cache_char = cache_char.Replace("space", " ");
                cache_char = cache_char.Replace("oemplus", "+");
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        private void richTextBox1_Resize(object sender, EventArgs e)
        {
            // auto-scroll
            richTextBox1.Select(richTextBox1.TextLength, 0);

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (!richTextBox1.Enabled)
            {
                // auto-scroll
                richTextBox1.Select(richTextBox1.TextLength, 0);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            foreach (ListViewItem _t in ((ListView)sender).Items)
            {
                if (_t.Selected)
                {
                    label1.Text = " Dettagli Evento";
                    richTextBox2.Text = "\n" + _t.SubItems[1].Text;
                }
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }
    }
}
