﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Reaper_Monitor
{
    public partial class Form1
    {
        private void EvenetLogUpdater()
        {
            try
            {
                System.Diagnostics.EventLog pEventLog = new System.Diagnostics.EventLog("Reaper Runtime Events");
                string[] dati = new string[5];
                int _numEntrys = 0;
                int _numNewEntrys = 0;
                //---->

                listView1.Columns[0].Width = (int)160;
                listView1.Columns[1].Width = (int)listView1.Width - 430;
                listView1.Columns[2].Width = (int)120;
                listView1.Columns[3].Width = (int)120;
                listView1.Refresh();

                //---->
                while (System.Threading.Thread.CurrentThread.ThreadState == System.Threading.ThreadState.Running)
                {
                    if (label1.Text == " Dettagli Evento")
                    {
                        _numNewEntrys = 0;
                    }

                    if (_numEntrys < pEventLog.Entries.Count)
                    {
                        this.listView1.Items.Clear();
                        foreach (System.Diagnostics.EventLogEntry entry in pEventLog.Entries)
                        {
                            dati[0] = entry.TimeWritten.ToLocalTime().ToString();
                            dati[1] = entry.Message;
                            dati[2] = entry.EntryType.ToString(); //livello
                            dati[3] = entry.Source;

                            if (listView1.SelectedIndices.Count > 0)
                            {
                                listView1.Items.Insert(listView1.SelectedIndices[0], new ListViewItem(dati));
                                listView1.Items[listView1.SelectedIndices[0]].Selected = false;
                            }
                            else listView1.Items.Add(new ListViewItem(dati));
                        }
                        listView1.Invalidate();
                    }

                    // Garbage Collection
                    System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                    // Salvo per ciclo successivo
                    _numNewEntrys += (pEventLog.Entries.Count - _numEntrys);
                    _numEntrys = pEventLog.Entries.Count;

                    if (_numNewEntrys > 0)
                    {
                        label1.Text = " Dettagli Evento: " + _numNewEntrys + " Nuov" + ((_numNewEntrys > 1) ? "i" : "o") + "!!";
                    }

                    // Garbage Collection
                    System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                    // Wait
                    System.Threading.Thread.Sleep(1000);
                }
            }
            catch (System.Exception)
            {
                Close();
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }
    }
}
