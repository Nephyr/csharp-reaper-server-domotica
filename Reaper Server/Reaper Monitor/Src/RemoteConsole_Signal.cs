﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Reaper_Monitor
{
    public partial class Form1
    {
        //
        //  CONSOLE REMOTA - PARSE
        //
        private void ExecuteCodeByCommand(string _txt)
        {
            string[] txt = new string[2];

            if (_txt.Contains(" "))
            {
                txt[0] = _txt.Split(' ')[0];
                for (int i = 1; i < _txt.Split(' ').Length; i++)
                {
                    txt[1] += _txt.Split(' ')[i];
                }
                txt[1] = txt[1].Trim();
                txt[1] = txt[1].Replace("+", " ");
            }
            else
            {
                txt[0] = _txt;
                txt[1] = "";
            }

            switch (txt[0].ToLower())
            {
                //
                //  NUMBER OF CLIENTS ONLINE
                //
                case "log":
                case "service":
                case "kick":
                case "get":
                    richTextBox1.AppendText("\n" + "Il comando immesso è di tipo: root.");
                    richTextBox1.AppendText("\n" + "Usare l'Helper a lato per visionare i sotto-comandi.");
                    break;

                //
                //  CLEAR LOG ENTRY
                //
                case "log.clear":
                    try
                    {
                        System.Diagnostics.EventLog pEventLog = new System.Diagnostics.EventLog("Reaper Runtime Events");
                        richTextBox1.AppendText("\n" + "Records rimossi dal Registro Eventi: " + pEventLog.Entries.Count);
                        pEventLog.Clear();
                        listView1.Items.Clear();
                        label1.Text = " Dettagli Evento";
                        richTextBox2.Text = @"
Con il Mouse 'Doppio Click' per visionare il testo di un evento.
Per avere la lista completa degli eventi andare su 'Event Log - Viewer'";
                    }
                    catch (System.Exception) { }
                    break;

                //
                //  NUMBER OF CLIENTS ONLINE
                //
                case "get.online":
                    if (pReaperController.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        richTextBox1.AppendText("\n" + "Impossibile eseguire il comando immesso." + "\n" +
                            "Lo stato corrente del servizio server non permette l'invio di segnali.");
                        return;
                    }
                    if (_RemoteConn.Connect())
                    {
                        _RemoteConn.SendPackage("SMSG_SQL_GET_ALL_ON_USERS[FINAL]", 5000);
                        string[] temp = _RemoteConn.ReceivePackage(8192, 5000).Split(',');
                        if (((int)(temp.Length / 6)) > 0)
                        {
                            richTextBox1.AppendText("\n" + "Accounts Online: " + ((int)(temp.Length / 6)));
                            for (int i = 0; i < temp.Length; i += 6)
                            {
                                richTextBox1.AppendText("\n\n" + "  >>  " + temp[i + 2] + "[ " + temp[i + 3] + ":" + temp[i + 4] + " ], Liv: " +
                                    temp[i + 1] + "\n" + "        Entry: " + temp[i] + ", StartTime: " + temp[i + 5]);
                            }
                            richTextBox1.AppendText("\n");
                        }
                        else
                        {
                            richTextBox1.AppendText("\n" + "Nessun account risulta su stato: online");
                        }
                    }
                    else
                    {
                        richTextBox1.AppendText("\n" + "Il monitor non risulta connesso al servizio");
                    }
                    _RemoteConn.Disconnect();
                    break;

                //
                //  ADD ACCOUNT
                //
                case "add.account":
                    if (pReaperController.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        richTextBox1.AppendText("\n" + "Impossibile eseguire il comando immesso." + "\n" +
                            "Lo stato corrente del servizio server non permette l'invio di segnali.");
                        return;
                    }
                    if (_RemoteConn.Connect())
                    {
                        _RemoteConn.SendPackage("SMSG_ACCOUNT_ADD " + txt[1].Replace(" ", "+") + "[FINAL]", 5000);
                        string _t = _RemoteConn.ReceivePackage(19, 5000);
                        if (_t == "SMSG_ACCOUNT_ADD_OK")
                        {
                            richTextBox1.AppendText("\n" + "Aggiunta account completata con successo");
                        }
                        else
                        {
                            richTextBox1.AppendText("\n" + "Impossibile aggiungere l'account specificato");
                        }
                    }
                    else
                    {
                        richTextBox1.AppendText("\n" + "Il monitor non risulta connesso al servizio");
                    }
                    _RemoteConn.Disconnect();
                    break;

                //
                //  UPDATE ACCOUNT'S PROFILE
                //
                case "update.profile":
                    if (pReaperController.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        richTextBox1.AppendText("\n" + "Impossibile eseguire il comando immesso." + "\n" +
                            "Lo stato corrente del servizio server non permette l'invio di segnali.");
                        return;
                    }
                    if (_RemoteConn.Connect())
                    {
                        _RemoteConn.SendPackage("SMSG_GET_ACCOUNT_PROFILE_UPDATE " + txt[1].Replace(" ", "+") + "[FINAL]", 5000);
                        string _t = _RemoteConn.ReceivePackage(34, 5000);
                        if (_t == "SMSG_GET_ACCOUNT_PROFILE_UPDATE_OK")
                        {
                            richTextBox1.AppendText("\n" + "Account aggiornato con successo");
                        }
                        else
                        {
                            richTextBox1.AppendText("\n" + "Impossibile aggiornare l'account specificato");
                        }
                    }
                    else
                    {
                        richTextBox1.AppendText("\n" + "Il monitor non risulta connesso al servizio");
                    }
                    _RemoteConn.Disconnect();
                    break;

                //
                //  DEL ACCOUNT
                //
                case "del.account":
                    if (pReaperController.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        richTextBox1.AppendText("\n" + "Impossibile eseguire il comando immesso." + "\n" +
                            "Lo stato corrente del servizio server non permette l'invio di segnali.");
                        return;
                    }
                    if (_RemoteConn.Connect())
                    {
                        _RemoteConn.SendPackage("SMSG_ACCOUNT_DEL " + txt[1].Replace(" ", "+") + "[FINAL]", 5000);
                        string _t = _RemoteConn.ReceivePackage(19, 5000);
                        if (_t == "SMSG_ACCOUNT_DEL_OK")
                        {
                            richTextBox1.AppendText("\n" + "Cancellazione account completata con successo");
                        }
                        else
                        {
                            richTextBox1.AppendText("\n" + "Impossibile rimuovere l'account specificato");
                        }
                    }
                    else
                    {
                        richTextBox1.AppendText("\n" + "Il monitor non risulta connesso al servizio");
                    }
                    _RemoteConn.Disconnect();
                    break;

                //
                //  LINKED ACCOUNT
                //
                case "get.linkto":
                    if (pReaperController.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        richTextBox1.AppendText("\n" + "Impossibile eseguire il comando immesso." + "\n" +
                            "Lo stato corrente del servizio server non permette l'invio di segnali.");
                        return;
                    }

                    if (_RemoteConn.Connect())
                    {
                        string acc = string.Empty;
                        if (txt.Length > 1)
                        {
                            acc = " " + txt[1];
                        }
                        else
                        {
                            acc = "";
                        }
                        _RemoteConn.SendPackage("SMSG_GET_ACCOUNT_LINKED" + acc + "[FINAL]", 5000);
                        string[] temp = _RemoteConn.ReceivePackage(8192, 5000).Split(',');
                        if (((int)(temp.Length / 5)) > 0)
                        {
                            richTextBox1.AppendText("\n" + "Accounts associati all'Entry(" +
                                                    ((acc != "") ? acc : " LOCALSYSTEM") + " ): " +
                                                    ((int)(temp.Length / 5)));
                            for (int i = 0; i < temp.Length; i += 5)
                            {
                                richTextBox1.AppendText("\n\n" + "  >>  " + temp[i + 2] + " " + temp[i + 3] + ", Online:  " +
                                    temp[i + 1] + "\n" + "        Entry: " + temp[i] + ", IP Address: " + temp[i + 4]);
                            }
                            richTextBox1.AppendText("\n");
                        }
                        else
                        {
                            richTextBox1.AppendText("\n" + "Nessun account associato all'Entry:" + ((acc != "") ? acc : " LOCALSYSTEM"));
                        }
                    }
                    else
                    {
                        richTextBox1.AppendText("\n" + "Il monitor non risulta connesso al servizio");
                    }
                    _RemoteConn.Disconnect();
                    break;

                //
                //  SEARCH ACCOUNT
                //
                case "get.account":
                    if (pReaperController.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        richTextBox1.AppendText("\n" + "Impossibile eseguire il comando immesso." + "\n" +
                            "Lo stato corrente del servizio server non permette l'invio di segnali.");
                        return;
                    }
                    if (_RemoteConn.Connect())
                    {
                        string acc = string.Empty;
                        if (txt.Length > 1)
                        {
                            acc = " " + txt[1];
                        }
                        else
                        {
                            acc = "";
                        }
                        _RemoteConn.SendPackage("SMSG_SEARCH_ACCOUNT" + acc + "[FINAL]", 5000);
                        string[] temp = _RemoteConn.ReceivePackage(8192, 5000).Split(',');
                        if (((int)(temp.Length / 4)) > 0)
                        {
                            richTextBox1.AppendText("\n" + "Occorrenze Accounts per(" +
                                                    ((acc != "") ? acc : " ALL") + " ): " +
                                                    ((int)(temp.Length / 4)));
                            int i = 0;
                            while (i < temp.Length)
                            {
                                richTextBox1.AppendText("\n\n" + "  >>  " + temp[i + 1] + " " + temp[i + 2] + "\n        Account: " + temp[i + 3]);
                                richTextBox1.AppendText("( " + temp[i] + " )");
                                i += 4;
                            }
                            richTextBox1.AppendText("\n");
                        }
                        else
                        {
                            richTextBox1.AppendText("\n" + "Nessun account trovato per:" + ((acc != "") ? acc : " ALL"));
                        }
                    }
                    else
                    {
                        richTextBox1.AppendText("\n" + "Il monitor non risulta connesso al servizio");
                    }
                    _RemoteConn.Disconnect();
                    break;

                //
                //  ACCOUNT PROFILE
                //
                case "get.profile":
                    if (pReaperController.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        richTextBox1.AppendText("\n" + "Impossibile eseguire il comando immesso." + "\n" +
                            "Lo stato corrente del servizio server non permette l'invio di segnali.");
                        return;
                    }
                    if (_RemoteConn.Connect())
                    {
                        _RemoteConn.SendPackage("SMSG_GET_ACCOUNT_PROFILE " + txt[1] + "[FINAL]", 5000);
                        string[] temp = _RemoteConn.ReceivePackage(8192, 5000).Split(',');
                        if (((int)(temp.Length / 9)) > 0)
                        {
                            richTextBox1.AppendText("\n" + ((int)(temp.Length / 9)) + " Profili totali per l'Entry: " + txt[1]);
                            for (int i = 0; i < temp.Length; i += 9)
                            {
                                richTextBox1.AppendText("\n\n" + "  >>  Entry: " + temp[i]);
                                richTextBox1.AppendText("\n        Level: " + temp[i + 1]);
                                richTextBox1.AppendText("\n        Link To: " + temp[i + 2]);
                                richTextBox1.AppendText("\n        Name: " + temp[i + 3]);
                                richTextBox1.AppendText("\n        Surname: " + temp[i + 4]);
                                richTextBox1.AppendText("\n        Telephone: " + temp[i + 5]);
                                richTextBox1.AppendText("\n        Address: " + temp[i + 6]);
                                richTextBox1.AppendText("\n        Role: " + temp[i + 7]);
                                richTextBox1.AppendText("\n        Reg.Data: " + temp[i + 8]);
                            }
                            richTextBox1.AppendText("\n");
                        }
                        else
                        {
                            richTextBox1.AppendText("\n" + "Nessun risultato per l'Entry: " + txt[1]);
                        }
                    }
                    else
                    {
                        richTextBox1.AppendText("\n" + "Il monitor non risulta connesso al servizio");
                    }
                    _RemoteConn.Disconnect();
                    break;

                //
                //  HISTORY CONNECTION
                //
                case "get.history":
                    if (pReaperController.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        richTextBox1.AppendText("\n" + "Impossibile eseguire il comando immesso." + "\n" +
                            "Lo stato corrente del servizio server non permette l'invio di segnali.");
                        return;
                    }
                    if (_RemoteConn.Connect())
                    {
                        _RemoteConn.SendPackage("SMSG_HISTORY_ACCOUNT " + txt[1] + "[FINAL]", 5000);
                        string[] temp = _RemoteConn.ReceivePackage(8192, 5000).Split(',');
                        if (((int)(temp.Length / 5)) > 0)
                        {
                            richTextBox1.AppendText("\n" + "Cronologia connessioni: " + ((int)(temp.Length / 5)));
                            for (int i = 0; i < temp.Length; i += 5)
                            {
                                richTextBox1.AppendText("\n\n" + "  >>  " + temp[i + 2] + ":" + temp[i + 3] +
                                    temp[i + 1] + "\n" + "        Entry: " + temp[i + 1] + ", StartTime: " + temp[i + 4]);
                            }
                            richTextBox1.AppendText("\n");
                        }
                        else
                        {
                            richTextBox1.AppendText("\n" + "Nessun connessione risulta per l'account: " + txt[1]);
                        }
                    }
                    else
                    {
                        richTextBox1.AppendText("\n" + "Il monitor non risulta connesso al servizio");
                    }
                    _RemoteConn.Disconnect();
                    break;

                //
                //  KICK CLIENTS ONLINE
                //
                case "kick.client":
                    if (pReaperController.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        richTextBox1.AppendText("\n" + "Impossibile eseguire il comando immesso." + "\n" +
                            "Lo stato corrente del servizio server non permette l'invio di segnali.");
                        return;
                    }
                    if (_RemoteConn.Connect())
                    {
                        _RemoteConn.SendPackage("SMSG_KICK_ACCOUNT " + txt[1] + "[FINAL]", 5000);
                        string temp = _RemoteConn.ReceivePackage(8192, 5000).Substring(0, 20);
                        if (temp == "SMSG_KICK_ACCOUNT_OK")
                        {
                            richTextBox1.AppendText("\n" + "Account Entry( " + txt[1] + " ) disconnesso con successo.");
                        }
                        else
                        {
                            richTextBox1.AppendText("\n" + "Errore durante la disconnessione dell'Account Entry( " +
                                txt[1] + " ).");
                        }
                    }
                    else
                    {
                        richTextBox1.AppendText("\n" + "Il monitor non risulta connesso al servizio");
                    }
                    _RemoteConn.Disconnect();
                    break;

                //
                //  START, STOP, RESTART
                //
                case "service.shutdown":
                    if (pReaperController.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        richTextBox1.AppendText("\n" + "Impossibile eseguire il comando immesso." + "\n" +
                            "Lo stato corrente del servizio server è già: Stopped...");
                        return;
                    }

                    richTextBox1.AppendText("\n" + "Arresto del servizio server." + "\n" + "Arresto in corso. Attendere prego...");
                    richTextBox1.Refresh();

                    pReaperController.Stop();
                    while (pReaperController.Status != System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        System.Threading.Thread.Sleep(1);
                        this.pReaperController.Refresh();
                    }
                    richTextBox1.AppendText("\n" + "Arresto del servizio [reaper.exe] completato con successo!");
                    break;

                case "service.start":
                    if (pReaperController.Status == System.ServiceProcess.ServiceControllerStatus.Running)
                    {
                        richTextBox1.AppendText("\n" + "Impossibile eseguire il comando immesso." + "\n" +
                            "Lo stato corrente del servizio server è già: Running...");
                        return;
                    }

                    richTextBox1.AppendText("\n" + "Avvio del servizio server." + "\n" + "Avvio in corso. Attendere prego...");
                    richTextBox1.Refresh();

                    pReaperController.Start();
                    while (pReaperController.Status != System.ServiceProcess.ServiceControllerStatus.Running)
                    {
                        System.Threading.Thread.Sleep(1);
                        this.pReaperController.Refresh();

                    }
                    richTextBox1.AppendText("\n" + "Avvio del servizio [reaper.exe] completato con successo!");
                    break;

                case "service.restart":
                    if (pReaperController.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        richTextBox1.AppendText("\n" + "Impossibile eseguire il comando immesso." + "\n" +
                            "Lo stato corrente del servizio server non permette il riavvio.");
                        return;
                    }

                    richTextBox1.AppendText("\n" + "Riavvio del servizio server." + "\n" + "Arresto in corso. Attendere prego...");
                    richTextBox1.Refresh();

                    pReaperController.Stop();
                    while (pReaperController.Status != System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        System.Threading.Thread.Sleep(1);
                        this.pReaperController.Refresh();

                    }
                    richTextBox1.AppendText("\n" + "Avvio in corso. Attendere prego...");
                    richTextBox1.Refresh();

                    pReaperController.Start();
                    while (pReaperController.Status != System.ServiceProcess.ServiceControllerStatus.Running)
                    {
                        System.Threading.Thread.Sleep(1);
                        this.pReaperController.Refresh();

                    }
                    richTextBox1.AppendText("\n" + "Servizio [reaper.exe] riavviato con successo!");
                    break;

                //
                //  CLEAR SCREEN
                //
                case "clear":
                    richTextBox1.Text = "Reaper - TCP Server Service [Versione 2.1.0.100]" +
                        "\n" + "Copyright (c) 2010 Vanzo Luca Samuele. Tutti i diritti riservati." + "\n";
                    label1.Text = " Dettagli Evento";
                    break;

                //
                //  COPY ALL TEXT
                //
                case "copy":
                    richTextBox1.SelectAll();
                    richTextBox1.Copy();
                    richTextBox1.Select(richTextBox1.TextLength, 0);
                    break;

                // DEFAULT VALUE = NULL
                default:
                    richTextBox1.AppendText("\n" + "Comando [" + txt[0] + ((txt[1] != "") ? " " + txt[1] : txt[1]) +
                        "] non valido per il servizio reaper.exe");
                    break;
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

            // Clear della cache
            cache_char = string.Empty;
        }
    }
}
