﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Reaper_Monitor
{
    public partial class Form1
    {
        //
        //  BTN ---> AZIONI.AVVIA
        //
        private void avviaserverToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.pReaperController.Refresh();
            if (this.pReaperController.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
            {
                this.pReaperController.Start();
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //
        //  BTN ---> AZIONI.RIAVVIA
        //
        private void riavviaServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.pReaperController.Refresh();
            if (this.pReaperController.Status == System.ServiceProcess.ServiceControllerStatus.Running)
            {
                this.pReaperController.Stop();
                while (this.pReaperController.Status != System.ServiceProcess.ServiceControllerStatus.Stopped) { }
                this.pReaperController.Start();
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //
        //  BTN ---> AZIONI.FERMA
        //
        private void fermaServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.pReaperController.Refresh();
            if (this.pReaperController.Status == System.ServiceProcess.ServiceControllerStatus.Running)
            {
                this.pReaperController.Stop();
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }
    }
}
