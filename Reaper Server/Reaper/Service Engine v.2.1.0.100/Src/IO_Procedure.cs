﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectClient
    {
        //===========================================================================================
        //
        //      DICHIARAZIONE FUNZIONE: INVIO PACCHETTI
        //
        public int SendPackage(string cBuffer, int timeout)
        {
            int startTickCount = 0;
            int sent = 0;
            int offset = 0;
            //---->
            byte[] buffer = System.Text.Encoding.ASCII.GetBytes(cBuffer);

            try
            {
                startTickCount = System.Environment.TickCount;
                _pClient._pSocketClient.SendTimeout = timeout;
                //---->
                do
                {
                    // FIX partizione carico in Multi-Threading
                    System.Threading.Thread.Sleep(1);

                    sent += _pClient._pSocketClient.Send(buffer, offset + sent, buffer.Length - sent,
                        System.Net.Sockets.SocketFlags.None);

                } while (sent < buffer.Length && !System.Text.Encoding.ASCII.GetString(buffer).Contains("[FINAL]"));
            }
            catch (System.Net.Sockets.SocketException)
            {
                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }

            return buffer.Length;
        }

        //===========================================================================================
        //
        //      DICHIARAZIONE FUNZIONE: RICEZIONE PACCHETTI
        //
        public byte[] ReceivePackage(int bytes, int timeout)
        {
            int received = 0;
            int offset = 0;
            byte[] buffer = new byte[bytes];
            //---->
            try
            {
                _pClient._pSocketClient.ReceiveTimeout = timeout;
                //---->
                do
                {
                    // FIX partizione carico in Multi-Threading
                    System.Threading.Thread.Sleep(1);

                    received += _pClient._pSocketClient.Receive(buffer, offset + received, buffer.Length - received,
                        System.Net.Sockets.SocketFlags.None);
                    
                } while (received < buffer.Length && !System.Text.Encoding.ASCII.GetString(buffer).Contains("[FINAL]"));
            }
            catch (System.Net.Sockets.SocketException)
            {
                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }

            return buffer;
        }

        //===========================================================================================
        //
        //      ISYNC: RECEIVE DATA
        //
        public string ISyncReceive(int timeout)
        {
            try
            {
                System.Collections.Generic.List<byte[]> StackReceive = new System.Collections.Generic.List<byte[]>();
                string buffer_to_string = string.Empty;
                int i = 0;
                //---->
                byte[] bytesPending = System.BitConverter.GetBytes(0);
                //---->
                StackReceive.Clear();
                do
                {
                    // FIX partizione carico in Multi-Threading
                    System.Threading.Thread.Sleep(1);

                    _pClient._pSocketClient.IOControl(System.Net.Sockets.IOControlCode.DataToRead, null, bytesPending);
                    StackReceive.Add(ReceivePackage(System.BitConverter.ToInt32(bytesPending, 0), timeout));
                } while (System.BitConverter.ToUInt32(bytesPending, 0) > 0 &&
                    !System.Text.Encoding.ASCII.GetString(StackReceive[StackReceive.Count - 1]).Contains("[FINAL]"));
                //---->
                buffer_to_string = string.Empty;
                for (i = 0; i < StackReceive.Count; i++)
                    buffer_to_string += System.Text.Encoding.ASCII.GetString(StackReceive[i]);

                return buffer_to_string.ToString().Replace("[FINAL]", "").Trim();
            }
            catch (System.Exception e)
            {
                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                _pEvents.EventWriteError(e);
                return string.Empty;
            }
        }
    }
}
