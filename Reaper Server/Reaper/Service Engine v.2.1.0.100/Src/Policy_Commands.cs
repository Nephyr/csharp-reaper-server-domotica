﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectClient
    {
        //
        //  OTTENGO I COMANDI PER IL LIVELLO UTENTE
        //
        private void RetriveAllCommandsByLevel()
        {
            try
            {
                string[] QueryResult;

                _pClient._AvailableCommands.Clear();
                QueryResult = _pClient.pSQLHandle.SQLReader(

                            // Eseguo la query per selezionare tutti gli account online
                            "SELECT b.syntax FROM policy a INNER JOIN commands b ON b.id_command = a.id_command WHERE a.id_level = " +
                            ((_pClient._pEntryPoint[2] != "") ? _pClient._pEntryPoint[2] : "-1") + " AND (b.type = 0 OR b.type) AND b.enable;"

                    ).Split(',');

                foreach (string _t in QueryResult)
                {
                    _pClient._AvailableCommands.Enqueue(_t.Trim());
                }
            }
            catch(System.Exception e)
            {
                _pEvents.EventWriteError(e);
                JoinToQueue(SMSGCMD.SMSG_SQL_QUERY_ERR.ToString());

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                return;
            }
        }

        //
        //  DETERMINO SE I COMANDI SONO PERMESSI PER IL LIVELLO UTENTE
        //
        private bool IsPermitted(string _cmd)
        {
            try
            {
                foreach (string _t in _pClient._AvailableCommands)
                {
                    if (_cmd.Trim() == _t)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (System.Exception)
            {
                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                return false;
            }
        }
    }
}
