﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectServer
    {
        private System.Net.Sockets.TcpListener _pTCPListener            = null;

        private bool _ServerIsStopped                                   = false;
        private bool _PruningIsStopped                                  = false;

        private System.Threading.Thread _pServerThread                  = null;
        private System.Threading.Thread _pPruningThread                 = null;
        
        private System.Collections.ArrayList _pListenerCollection       = null;

        private SQL _SQL                                                = null;

        //===========================================================================================

        public EventLogger _pEvents = new EventLogger();

        //===========================================================================================

        public TCPObjectServer()
        {
            try
            {
                InitConfigService();
                Init();
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        private void Init()
        {
            try
            {
                _pTCPListener       = new System.Net.Sockets.TcpListener(new System.Net.IPEndPoint(
                                                                   pConfig.GameWorldBindIP,
                                                                   pConfig.GameWorldPortServer));

                _SQL                = new SQL( pConfig.MySQL_HostDatabase,
                                               pConfig.MySQL_DBTable,
                                               pConfig.MySQL_Username,
                                               pConfig.MySQL_Password );
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                _pTCPListener = null;
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        ~TCPObjectServer()
		{
            try
            {
                StopServer();
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
		}
    }
}
