﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectServer
    {
        private void StopAllSocketListers()
        {
            try
            {
                // Ciclo di Running
                foreach (TCPObjectClient _client in _pListenerCollection)
                {
                    _client.StopSocketListener();
                }

                // Remove all elements from the list.
                _pListenerCollection.Clear();
                _pListenerCollection = null;
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        private void _tServerProcedure()
        {
            try
            {
                // Client Socket variable;
                System.Net.Sockets.Socket clientSocket = null;

                // Ciclo di Running
                while (!_ServerIsStopped)
                {
                    // FIX partizione carico in Multi-Threading
                    System.Threading.Thread.Sleep(1);
                    
                    if(_ServerIsStopped)
                    {
                        // Garbage Collection
                        System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                        // FIX Exception: _pTCPListener.Pending();
                        return;
                    }

                    if (_pTCPListener.Pending())
                    {
                        // Attendo per un nuovo client
                        clientSocket = _pTCPListener.AcceptSocket();

                        // Creo un oggetto per il client
                        TCPObjectClient _client = new TCPObjectClient(clientSocket, pConfig);

                        // Aggiungo alla Collezione il Client corrente
                        lock (_pListenerCollection)
                        {
                            _pListenerCollection.Add(_client);
                        }

                        // Avvio la comunicazione in un'altra istanza
                        _client.StartSocketListener();

                        // Garbage Collection
                        System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
                    }
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                _ServerIsStopped = true;
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
	    }

        //===========================================================================================

	    private void _tPruningProcedure()
        {
            try
            {
                System.Collections.ArrayList deleteList = new System.Collections.ArrayList();
                while (!_PruningIsStopped)
                {
                    //Controllo tutti i Client presenti nella Collezione
                    lock (_pListenerCollection)
                    {
                        string temp = _SQL.SQLReader(

                                // Eseguo la query per selezionare tutti gli account online
                                "SELECT a.id_account FROM accounts a WHERE a.online;"

                        );

                        foreach (TCPObjectClient _client in _pListenerCollection)
                        {
                            if (_client._pClient._ClientIsLogged && !temp.Contains(_client._pClient._pEntryPoint[0]))
                            {
                                // Se il client risulta Online, ma flaggato Offline su Database,
                                // viene segnalato per la terminazione
                                _client.MarkForDeletion();
                            }

                            if (_client.IsMarkedForDeletion())
                            {
                                deleteList.Add(_client);
                                _client.StopSocketListener();

                                // FIX: Anti-Account Freeze
                                _SQL.SQLGetAffected("UPDATE accounts a SET a.online = 0 WHERE a.id_account = '" +
                                    _client._pClient._pEntryPoint[0] + "';", true);
                            }
                        }

                        // Rimuovo tutti i Client marcati IsDeleted
                        for (int i = 0; i < deleteList.Count; ++i)
                        {
                            _pListenerCollection.Remove(deleteList[i]);
                        }
                    }

                    // Garbage Collection
                    System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                    // Pulisco la lista specchio e mando in attesa il thread (eseguo altro)
                    deleteList.Clear();

                    // Attendo 1 secondo prima di rieseguire
                    System.Threading.Thread.Sleep(2000);
                }
            }
            catch(System.Threading.ThreadAbortException){}
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
            }
            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }
    }
}
