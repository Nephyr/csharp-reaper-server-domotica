﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectServer
    {
        // STARTER Declaration.
        public void StartServer()
        {
            try
            {
                if (_pTCPListener != null)
                {
                    // Inizializzo la collezione di Listener
                    _pListenerCollection = new System.Collections.ArrayList();
                    _pTCPListener.Start();

                    // Inizializzo tutti gli account a Offline
                    _SQL.SQLGetAffected("UPDATE accounts a SET a.online = 0;", false);

                    // Avvio del thread ---> AcceptClient
                    _pServerThread = new System.Threading.Thread(new System.Threading.ThreadStart(_tServerProcedure));
                    _pServerThread.Start();
                    _pServerThread.Priority = System.Threading.ThreadPriority.Highest;

                    // Avvio del thread ---> Pruning (potatura) della collezione se marcato IsDeleted
                    _pPruningThread = new System.Threading.Thread(new System.Threading.ThreadStart(_tPruningProcedure));
                    _pPruningThread.Start();
                    _pPruningThread.Priority = System.Threading.ThreadPriority.Lowest;
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                _pTCPListener = null;
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        // STOPPER Declaration
        public void StopServer()
        {
            try
            {
                if (_pTCPListener != null)
                {
                    // Stop TCP/IP Server.
                    _ServerIsStopped = true;
                    _pTCPListener.Stop();

                    _pServerThread.Join(1000);
                    if (_pServerThread.IsAlive)
                    {
                        _pServerThread.Abort();
                    }
                    _pServerThread = null;
                    _PruningIsStopped = true;

                    _pPruningThread.Join(1000);
                    if (_pPruningThread.IsAlive)
                    {
                        _pPruningThread.Abort();
                    }
                    _pPruningThread = null;
                    _pTCPListener = null;

                    // Fermo tutti i clients
                    StopAllSocketListers();
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                _pTCPListener = null;
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }
    }
}
