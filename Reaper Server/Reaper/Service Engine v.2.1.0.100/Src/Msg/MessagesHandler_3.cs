﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectClient
    {
        //===========================================================================================
        //
        //      HANDLE MSG: SMSG_GET_ACCOUNT_PROFILE
        //
        private void smsg_get_account_profile_update(string id_acc)
        {
            try
            {
                if (_pClient._ClientIsLogged)
                {
                    if (id_acc.Contains(" "))
                    {
                        string[] data = id_acc.Split(' ')[1].Split(',');
                        int temp = 0;

                        if (data.Length > 7)
                        {
                            temp = _pClient.pSQLHandle.SQLGetAffected(

                                 "UPDATE accounts SET user = '" + data[1].Replace("+", " ") + "', pwd = '" + data[2].Replace("+", " ") +
                                 "', name = '" + data[3].Replace("+", " ") + "', surname = '" + data[4].Replace("+", " ") +
                                 "', tel_number = '" + data[5].Replace("+", " ") + "', address = '" + data[6].Replace("+", " ") +
                                 "', role = '" + data[7].Replace("+", " ") + "' WHERE id_account = " + data[0] + " AND id_level < " +
                                 _pClient._pEntryPoint[2] + ";", true);
                        }

                        if (temp > 0)
                        {
                            smsg_get_account_profile("SMSG_GET_ACCOUNT_PROFILE " + data[0]);
                            //---->
                            _pEvents.EventWriteInformation("Comando: SMSG_GET_ACCOUNT_PROFILE_UPDATE" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + id_acc.Length +
                            "\n" + "Command.Info::Text: " + id_acc.Split(' ')[1]);
                        }
                        else
                        {
                            JoinToQueue(SMSGCMD.SMSG_GET_ACCOUNT_PROFILE_ERR.ToString() + "[FINAL]");
                            _pEvents.EventWriteFailureAudit("Comando: SMSG_GET_ACCOUNT_PROFILE_UPDATE" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + SMSGCMD.SMSG_GET_ACCOUNT_PROFILE_ERR.ToString().Length +
                            "\n" + "Command.Info::Error: " + SMSGCMD.SMSG_GET_ACCOUNT_PROFILE_ERR.ToString() +
                            "\n" + "Command.Info::Text: " + id_acc.Split(' ')[1]);
                        }
                    }
                    else
                    {
                        JoinToQueue(SMSGCMD.SMSG_GET_ACCOUNT_PROFILE_ERR.ToString() + "[FINAL]");
                        _pEvents.EventWriteFailureAudit("Comando: SMSG_GET_ACCOUNT_PROFILE_UPDATE" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + SMSGCMD.SMSG_GET_ACCOUNT_PROFILE_ERR.ToString().Length +
                            "\n" + "Command.Info::Text: " + SMSGCMD.SMSG_GET_ACCOUNT_PROFILE_ERR.ToString() +
                            "\n" + "Command.Info::Text: " + id_acc);
                    }
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                JoinToQueue(SMSGCMD.SMSG_EXCEPTION_ERROR.ToString());

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
            //---->
            _pClient._ClientIsKeepAlive = true;
        }

        //===========================================================================================
        //
        //      HANDLE MSG: SMSG_SESSION_DATA_REG_ADD
        //
        private void smsg_session_data_reg_add(string id_acc)
        {
            try
            {
                if (_pClient._ClientIsLogged)
                {
                    int QueryResult = 0;

                    if (id_acc.Split(' ').Length > 1)
                    {
                        if (IsPermitted(id_acc.Split(' ')[1]) || _pClient._pEntryPoint[2] == "5")
                        {
                            string _temp = string.Empty;
                            string[] _data = id_acc.Split(' ')[2].Split(',');
                            string[] date = System.DateTime.Now.ToLocalTime().ToString().Split(' ')[0].Split('/');

                            _temp = _pClient.pSQLHandle.SQLReader(
                                            "SELECT a.id_command FROM commands a WHERE a.syntax = '" + id_acc.Split(' ')[1] + "' LIMIT 1;");

                            QueryResult = _pClient.pSQLHandle.SQLGetAffected(
                                            "INSERT INTO data_sessions(id_connection, id_command, results, log, data) " +
                                            "VALUES(" + _pClient._pEntryPoint[3] + ", " + _temp + ", '" +
                                            _data[0].Replace("+", " ") + "', '" + _data[1].Replace("+", " ") +
                                            "', '" + date[2] + "/" + date[1] + "/" + date[0] + " " +
                                            System.DateTime.Now.ToLocalTime().ToString().Split(' ')[1] + "');", true);
                        }
                    }

                    if (QueryResult > 0)
                    {
                        JoinToQueue(SMSGCMD.SMSG_SESSION_DATA_REG_OK.ToString());
                        _pEvents.EventWriteSuccessAudit("Comando: SMSG_SESSION_DATA_REG_ADD" + "\n" +
                                  "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                                  " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                                  "\n" + "Command.Info::Len: " + ((id_acc.Split(' ')[1].Length + id_acc.Split(' ')[2].Length + 1)) +
                                  "\n" + "Command.Info::Text: " + id_acc.Split(' ')[1] + " " + id_acc.Split(' ')[2]);
                    }
                    else
                    {
                        JoinToQueue(SMSGCMD.SMSG_SESSION_DATA_REG_ERR.ToString());
                        _pEvents.EventWriteFailureAudit("Comando: SMSG_SESSION_DATA_REG_ADD" + "\n" +
                                 "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                                 " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                                 "\n" + "Command.Info::Error: " + SMSGCMD.SMSG_SESSION_DATA_REG_ERR.ToString() +
                                 "\n" + "Command.Info::Text: " + id_acc.Split(' ')[1] + " " + id_acc.Split(' ')[2]);
                    }
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                JoinToQueue(SMSGCMD.SMSG_EXCEPTION_ERROR.ToString());

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
            //---->
            _pClient._ClientIsKeepAlive = true;
        }
    }
}
