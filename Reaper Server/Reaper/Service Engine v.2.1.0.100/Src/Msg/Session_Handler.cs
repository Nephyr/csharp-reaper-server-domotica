﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectClient
    {
        //
        //  REGISTRO COMANDI UTENTI
        //
        private void RegisterCommandToUser(string cmd)
        {
            try
            {
                if (_pClient._pEntryPoint[2] == "5")
                    return;

                if (cmd == SMSGCMD.SMSG_KEEP_ALIVE.ToString() ||
                    cmd == SMSGCMD.SMSG_LOGOUT_ACCOUNT.ToString())
                    return;

                int QueryResult;
                string _temp = string.Empty;
                string[] data = System.DateTime.Now.ToLocalTime().ToString().Split(' ')[0].Split('/');

                _temp = _pClient.pSQLHandle.SQLReader(
                              "SELECT a.id_command FROM commands a WHERE a.syntax = '" + cmd + "' LIMIT 1;");

                QueryResult = _pClient.pSQLHandle.SQLGetAffected(
                                "INSERT INTO data_sessions(id_connection, id_command, data) " +
                                "VALUES(" + _pClient._pEntryPoint[3] + ", " + _temp +
                                ", '" + data[2] + "/" + data[1] + "/" + data[0] + " " +
                                System.DateTime.Now.ToLocalTime().ToString().Split(' ')[1] + "')", true);

                if (QueryResult <= 0)
                {
                    JoinToQueue(SMSGCMD.SMSG_SESSION_DATA_REG_ERR.ToString());
                    _pEvents.EventWriteFailureAudit("Comando: SMSG_SESSION_DATA_REG" + "\n" +
                             "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                             " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                             "\n" + "Command.Info::Error: " + SMSGCMD.SMSG_SESSION_DATA_REG_ERR.ToString());
                }
            }
            catch(System.Exception e)
            {
                _pEvents.EventWriteError(e);
                JoinToQueue(SMSGCMD.SMSG_SQL_QUERY_ERR.ToString());

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                return;
            }
        }
    }
}
