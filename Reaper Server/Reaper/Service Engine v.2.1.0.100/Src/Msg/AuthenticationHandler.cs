﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectClient
    {
        //===========================================================================================
        //
        //      ISYNC: PROTOCOLLO DI AUTENTICAZIONE [Login]
        //
        public bool ClientLogin()
        {
            try
            {
                string[] AUTH_STR_CHK = _pClient._InQueue.Peek().Replace("[FINAL]", "").Trim().Split(' ');
                string _temp = string.Empty;
                string _temp_conn = string.Empty;
                int success_reg = 0;
                //---->
                if (_pClient.pSQLHandle.SQLGetAffected(
                    "SELECT id_account FROM accounts a WHERE a.pwd = '" + AUTH_STR_CHK[1].ToString() + "' AND NOT a.online LIMIT 1;", false) > 0)
                {
                    if (_pClient.pSQLHandle.SQLGetAffected(
                        "UPDATE accounts a SET a.online = 1 WHERE a.pwd = '" + AUTH_STR_CHK[1].ToString() + "';", true) > 0)
                    {
                        _temp = _pClient.pSQLHandle.SQLReader(
                            "SELECT id_account, user, id_level FROM accounts a WHERE a.online AND a.pwd = '" + AUTH_STR_CHK[1].ToString() +
                            "' LIMIT 1;");

                        _pClient._pEntryPoint[0] = _temp.Split(',')[0];
                        _pClient._pEntryPoint[1] = _temp.Split(',')[1];
                        _pClient._pEntryPoint[2] = _temp.Split(',')[2];

                        //---->
                        if (_pClient._pEntryPoint[2] != "5")
                        {
                            success_reg = _pClient.pSQLHandle.SQLGetAffected(
                                "INSERT INTO connections(id_account, address, port) VALUES(" + _pClient._pEntryPoint[0] +
                                ", '" + _pClient.RemoteEndPoint().ToString().Split(':')[0] +
                                "', '" + _pClient.RemoteEndPoint().ToString().Split(':')[1] + "')", true);
                        }
                        else success_reg = 1;
                        //---->
                        if (success_reg > 0)
                        {
                            _temp_conn = _pClient.pSQLHandle.SQLReader(
                                  "SELECT a.id_connection, a.data FROM connections a WHERE a.id_account = " + _pClient._pEntryPoint[0] +
                                  " ORDER BY a.data DESC LIMIT 1;");

                            _pClient._pEntryPoint[3] = _temp_conn.Split(',')[0];
                            
                            _pClient._OutQueue.Enqueue(SMSGCMD.SMSG_LOGIN_ACCOUNT_OK.ToString());
                            if (_pClient._pEntryPoint[2] != "5")
                            {
                                _pEvents.EventWriteSuccessAudit("Auth.LoginData::Host< Remote.EndPoint( " + _pClient.RemoteEndPoint() + " ), Online >\n" +
                                "Auth.LoginData::Set< AccName( " + _pClient._pEntryPoint[1] + " ), AccEntry( " + _pClient._pEntryPoint[0] + 
                                " ), AccLevel( " + _pClient._pEntryPoint[2] + " ) >");
                            }
                            return true;
                        }
                        else
                        {
                            _pClient._OutQueue.Enqueue(SMSGCMD.SMSG_LOGIN_ACCOUNT_REGCONN_ERR.ToString());
                            _pEvents.EventWriteFailureAudit("Auth.LoginData::Host< Remote.EndPoint( " + _pClient.RemoteEndPoint() +
                                " ), SMSG_LOGIN_ACCOUNT_REGCONN_ERR >");
                            return false;
                        }
                    }
                    else
                    {
                        _pClient._OutQueue.Enqueue(SMSGCMD.SMSG_LOGIN_ACCOUNT_UPDATE_ERR.ToString());
                        _pEvents.EventWriteFailureAudit("Auth.LoginData::Host< Remote.EndPoint( " + _pClient.RemoteEndPoint() +
                            " ), SMSG_LOGIN_ACCOUNT_UPDATE_ERR >");
                        return false;
                    }
                }
                else
                {
                    if (_pClient.pSQLHandle.SQLGetAffected(
                        "SELECT id_account FROM accounts a WHERE a.pwd = '" + AUTH_STR_CHK[1].ToString() + "' AND a.online LIMIT 1;", false) > 0)
                    {
                        _pClient._OutQueue.Enqueue(SMSGCMD.SMSG_LOGIN_ACC_MIRROR_ERR.ToString());
                        _pEvents.EventWriteWarning("Auth.LoginData::Host< Remote.EndPoint( " + _pClient.RemoteEndPoint() +
                            " ), SMSG_LOGIN_ACC_MIRROR_ERR >");
                        return false;
                    }
                    else
                    {
                        _pClient._OutQueue.Enqueue(SMSGCMD.SMSG_LOGIN_ACCOUNT_ERR.ToString());
                        _pEvents.EventWriteWarning("Auth.LoginData::Host< Remote.EndPoint( " + _pClient.RemoteEndPoint() +
                            " ), SMSG_LOGIN_ACCOUNT_ERR >");
                        return false;
                    }
                }
            }
            catch (System.Exception)
            {
                _pClient._OutQueue.Enqueue(SMSGCMD.SMSG_LOGIN_ACCOUNT_ERR.ToString());
                _pEvents.EventWriteWarning("Auth.LoginData::Host< Remote.EndPoint( " + _pClient.RemoteEndPoint() +
                    " ), SMSG_LOGIN_ACCOUNT_ERR >");

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                return false;
            }
        }

        //===========================================================================================
        //
        //      ISYNC: PROTOCOLLO DI AUTENTICAZIONE [Logout]
        //
        public bool ClientLogout()
        {
            try
            {
                if (_pClient.pSQLHandle.SQLGetAffected(
                    "UPDATE accounts a SET a.online = 0 WHERE a.id_account = '" +
                    _pClient._pEntryPoint[0] + "';", true) > 0)
                {
                    if (_pClient._pEntryPoint[2] != "5")
                    {
                        _pEvents.EventWriteSuccessAudit("Auth.LogoutData::Host< Remote.EndPoint( " + _pClient.RemoteEndPoint() +
                            " ), Remote.Account( " + _pClient._pEntryPoint[1] + " ), Offline >");
                    }
                    return true;
                }
                else
                {
                    _pClient._OutQueue.Enqueue(SMSGCMD.SMSG_LOGOUT_ACCOUNT_ERR.ToString());
                    _pEvents.EventWriteFailureAudit("Auth.LogoutData::Host< Remote.EndPoint( " + _pClient.RemoteEndPoint() +
                        " ), SMSG_LOGOUT_ACCOUNT_ERR >");
                    return false;
                }
            }
            catch (System.Exception)
            {
                _pClient._OutQueue.Enqueue(SMSGCMD.SMSG_LOGOUT_ACCOUNT_ERR.ToString());
                _pEvents.EventWriteFailureAudit("Auth.LogoutData::Host< Remote.EndPoint( " + _pClient.RemoteEndPoint() +
                           " ), SMSG_LOGOUT_ACCOUNT_ERR >");

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                return false;
            }
        }
    }
}
