﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectClient
    {
        //===========================================================================================
        //
        //      HANDLE MSG: SMSG_ACCOUNT_LOGIN
        //
        private void smsg_account_login()
        {
            try
            {
                if (!_pClient._ClientIsLogged)
                {
                    if (!ClientLogin())
                    {
                        _pClient._ClientIsRunning = false;
                    }
                    else
                    {
                        _pClient._ClientIsLogged = true;
                        _pClient._ClientIsKeepAlive = true;
                    }
                }
            }
            catch (System.Exception e)
            {
                _pClient._ClientIsRunning = false;
                _pEvents.EventWriteError(e);

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
        }

        //===========================================================================================
        //
        //      HANDLE MSG: SMSG_GET_ACCOUNT_LINKED
        //
        private void smsg_account_linked_to(string id_acc)
        {
            try
            {
                if (_pClient._ClientIsLogged)
                {
                    string idtemp = string.Empty;
                    System.Int32 isLast = 0;
                    string QueryResult;
                    string[] temp;
                    //---->
                    if (_pClient._pEntryPoint[2] == "5")
                    {
                        idtemp = id_acc;
                    }
                    else
                    {
                        idtemp = ((_pClient._pEntryPoint[0] != "") ? _pClient._pEntryPoint[0] : "-1");
                    }
                    //---->
                    QueryResult = string.Empty;
                    temp = _pClient.pSQLHandle.SQLReader(

                        // Eseguo la query per selezionare tutti gli account online
                        "SELECT a.id_account FROM accounts a WHERE a.linked_to = " + idtemp + " AND a.id_level < " + _pClient._pEntryPoint[2] + ";"

                        ).Split(',');
                    //---->
                    isLast = temp.Length;
                    foreach (string _s in temp)
                    {
                        isLast--;
                        QueryResult += _pClient.pSQLHandle.SQLReader(

                             // Eseguo la query per accodare informazioni al vettore
                             "SELECT a.id_account, a.online, a.name, a.surname, b.address FROM accounts a " +
                             "LEFT JOIN connections b ON (a.id_account = b.id_account) " +
                             "WHERE a.id_account = " + _s + " AND a.id_level < " +
                             _pClient._pEntryPoint[2] + " ORDER BY b.data DESC LIMIT 1;"

                        ) + ((isLast > 0) ? "," : "");
                    }
                    //---->
                    if (QueryResult != "CX:0x01SQLERR")
                    {
                        JoinToQueue(QueryResult + ((_pClient._pEntryPoint[2] == "5") ? "[FINAL]" : ""));
                        _pEvents.EventWriteInformation("Comando: SMSG_GET_ACCOUNT_LINKED" + "\n"+
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + QueryResult.Length +
                            "\n" + "Command.Info::Text: " + QueryResult);
                    }
                    else
                    {
                        JoinToQueue(SMSGCMD.SMSG_GET_ACCOUNT_LINKED_ERR.ToString() + "[FINAL]");
                        _pEvents.EventWriteFailureAudit("Comando: SMSG_GET_ACCOUNT_LINKED" + "\n"+
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + SMSGCMD.SMSG_GET_ACCOUNT_LINKED_ERR.ToString().Length +
                            "\n" + "Command.Info::Text: " + SMSGCMD.SMSG_GET_ACCOUNT_LINKED_ERR.ToString());
                    }
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                JoinToQueue(SMSGCMD.SMSG_EXCEPTION_ERROR.ToString());

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
            //---->
            _pClient._ClientIsKeepAlive = true;
        }

        //===========================================================================================
        //
        //      HANDLE MSG: SMSG_SEARCH_ACCOUNT
        //
        private void smsg_account_search_by(string id_acc)
        {
            try
            {
                if (_pClient._ClientIsLogged)
                {
                    string[] term = id_acc.Split(' ');
                    string search = string.Empty;
                    string temp = string.Empty;

                    if (id_acc.Contains(" "))
                    {
                        search = "(";
                        for (int i = 1; i < term.Length; i++)
                        {
                            search += "(a.name LIKE '%" + term[i] + "%' OR a.surname LIKE '%" + term[i] + "%')" + ((i != term.Length - 1) ? "OR" : "");
                        }
                        search = search.Trim() + ") AND ";
                    }
                    else search = "";

                    temp = _pClient.pSQLHandle.SQLReader(
                          "SELECT a.id_account, a.name, a.surname, a.user FROM accounts a" +
                          " WHERE " + search + " a.id_level < " + _pClient._pEntryPoint[2] + " ORDER BY a.name, a.surname;");

                    if (temp != "CX:0x01SQLERR")
                    {
                        JoinToQueue(temp + ((_pClient._pEntryPoint[2] == "5") ? "[FINAL]" : ""));
                        _pEvents.EventWriteInformation("Comando: SMSG_SEARCH_ACCOUNT" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + temp.Length +
                            "\n" + "Command.Info::Text: " + temp);
                    }
                    else
                    {
                        JoinToQueue(SMSGCMD.SMSG_SEARCH_ACCOUNT_ERR.ToString() + "[FINAL]");
                        _pEvents.EventWriteFailureAudit("Comando: SMSG_GET_ACCOUNT_LINKED" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + SMSGCMD.SMSG_SEARCH_ACCOUNT_ERR.ToString().Length +
                            "\n" + "Command.Info::Text: " + SMSGCMD.SMSG_SEARCH_ACCOUNT_ERR.ToString());
                    }
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                JoinToQueue(SMSGCMD.SMSG_EXCEPTION_ERROR.ToString());

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
            //---->
            _pClient._ClientIsKeepAlive = true;
        }

        //===========================================================================================
        //
        //      HANDLE MSG: SMSG_SQL_GET_ALL_ON_USERS
        //
        private void smsg_sql_get_all_on_users()
        {
            try
            {
                if (_pClient._ClientIsLogged)
                {
                    System.Int32 isLast = 0;
                    string QueryResult;
                    string[] temp;
                    //---->
                    QueryResult = string.Empty;
                    temp = _pClient.pSQLHandle.SQLReader(

                        // Eseguo la query per selezionare tutti gli account online
                        "SELECT a.id_account FROM accounts a WHERE a.online AND a.id_level < " + _pClient._pEntryPoint[2] + ";"

                        ).Split(',');

                    isLast = temp.Length;
                    foreach (string _s in temp)
                    {
                        isLast--;
                        if (_pClient._pEntryPoint[2] == "5")
                        {
                            QueryResult += _pClient.pSQLHandle.SQLReader(

                                // Eseguo la query per accodare informazioni al vettore
                                "SELECT a.id_account, a.id_level, a.user, b.address, b.port, b.data FROM accounts a " +
                                "INNER JOIN connections b ON (a.id_account = b.id_account) " +
                                "WHERE a.id_account = " + _s + " AND a.id_level < " +
                                _pClient._pEntryPoint[2] + " ORDER BY b.data DESC LIMIT 1;"

                            ) + ((isLast > 0) ? "," : "[FINAL]");
                        }
                        else
                        {
                            QueryResult += _pClient.pSQLHandle.SQLReader(

                               // Eseguo la query per accodare informazioni al vettore
                               "SELECT a.id_account, a.user, b.address, b.port, b.data FROM accounts a " +
                               "INNER JOIN connections b ON (a.id_account = b.id_account) " +
                               "WHERE a.id_account = " + _s + " AND a.id_level < " +
                               _pClient._pEntryPoint[2] + " ORDER BY b.data DESC LIMIT 1;"

                                ) + ((isLast > 0) ? "," : "");
                        }
                    }
                    //---->
                    if (QueryResult != "CX:0x01SQLERR") JoinToQueue(QueryResult);
                    else JoinToQueue(SMSGCMD.SMSG_SQL_QUERY_ERR.ToString());
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                JoinToQueue(SMSGCMD.SMSG_EXCEPTION_ERROR.ToString());

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
            //---->
            _pClient._ClientIsKeepAlive = true;
        }
    }
}
