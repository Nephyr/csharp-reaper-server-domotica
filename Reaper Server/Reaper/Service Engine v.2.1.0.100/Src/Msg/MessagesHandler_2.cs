﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectClient
    {
        //===========================================================================================
        //
        //      HANDLE MSG: SMSG_ACCOUNT_REMOVE
        //
        private void smsg_account_del(string id_acc)
        {
            try
            {
                if (_pClient._ClientIsLogged)
                {
                    int temp = _pClient.pSQLHandle.SQLGetAffected(
                          "DELETE FROM accounts WHERE id_account IN(" + id_acc + ") AND id_level < " + _pClient._pEntryPoint[2] + ";", true);

                    if (temp > 0)
                    {
                        JoinToQueue(SMSGCMD.SMSG_ACCOUNT_DEL_OK.ToString() + ((_pClient._pEntryPoint[2] == "5") ? "[FINAL]" : ""));
                        _pEvents.EventWriteInformation("Comando: SMSG_ACCOUNT_DEL" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Affected: " + temp +
                            "\n" + "Command.Info::ListAccount: " + id_acc);
                    }
                    else
                    {
                        JoinToQueue(SMSGCMD.SMSG_ACCOUNT_DEL_ERR.ToString() + ((_pClient._pEntryPoint[2] == "5") ? "[FINAL]" : ""));
                        _pEvents.EventWriteFailureAudit("Comando: SMSG_ACCOUNT_DEL" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + SMSGCMD.SMSG_ACCOUNT_DEL_ERR.ToString().Length +
                            "\n" + "Command.Info::Text: " + SMSGCMD.SMSG_ACCOUNT_DEL_ERR.ToString());
                    }
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                JoinToQueue(SMSGCMD.SMSG_EXCEPTION_ERROR.ToString());

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
            //---->
            _pClient._ClientIsKeepAlive = true;
        }

        //===========================================================================================
        //
        //      HANDLE MSG: SMSG_ACCOUNT_ADD
        //
        private void smsg_account_add(string add_acc)
        {
            try
            {
                if (_pClient._ClientIsLogged)
                {
                    if (add_acc.Contains(" "))
                    {
                        string[] term = add_acc.Split(' ')[1].Split(',');
                        string insert = string.Empty;

                        for (int i = 0; i < term.Length; i+=8)
                        {
                            string[] data = System.DateTime.Now.ToLocalTime().ToString().Split(' ')[0].Split('/');
                            insert += "(" + term[i] + ", '" + term[i + 1].Replace("+", " ") +
                                "', '" + term[i + 2].Replace("+", " ") + "', 0, " + ((term[i + 3] == "null") ? _pClient._pEntryPoint[0] : term[i + 3]) +
                                      ", '" + term[i + 4].Replace("+", " ") + "', '" + term[i + 5].Replace("+", " ") + "', '" +
                                      term[i + 6].Replace("+", " ") + "', '" + term[i + 7].Replace("+", " ") +
                                      "', '" + data[2] + "/" + data[1] + "/" + data[0] + " " +
                                      System.DateTime.Now.ToLocalTime().ToString().Split(' ')[1] + "')" +
                                      (((i + 8) != term.Length) ? "," : "");
                        }

                        int affected = _pClient.pSQLHandle.SQLGetAffected(
                              "INSERT INTO accounts(id_level, user, pwd, online, linked_to, name, surname, tel_number, address, data)" +
                              " VALUES" + insert, true);
                        
                        if (affected > 0)
                        {
                            JoinToQueue(SMSGCMD.SMSG_ACCOUNT_ADD_OK.ToString() + "[FINAL]");
                            _pEvents.EventWriteInformation("Comando: SMSG_ACCOUNT_ADD" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + add_acc.Split(' ')[1].Length +
                            "\n" + "Command.Info::Text: " + add_acc.Split(' ')[1] +
                            "\n" + "Command.Info::Affected: " + affected);
                        }
                        else
                        {
                            JoinToQueue(SMSGCMD.SMSG_ACCOUNT_ADD_ERR.ToString() + "[FINAL]");
                            _pEvents.EventWriteFailureAudit("Comando: SMSG_ACCOUNT_ADD" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Error: " + SMSGCMD.SMSG_ACCOUNT_ADD_ERR.ToString() +
                            "\n" + "Command.Info::Text: " + add_acc.Split(' ')[1] +
                            "\n" + "Command.Info::Affected: " + affected);
                        }
                    }
                    else
                    {
                        JoinToQueue(SMSGCMD.SMSG_ACCOUNT_ADD_ERR.ToString() + "[FINAL]");
                        _pEvents.EventWriteFailureAudit("Comando: SMSG_ACCOUNT_ADD" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Error: " + SMSGCMD.SMSG_ACCOUNT_ADD_ERR.ToString() +
                            "\n" + "Command.Info::Text: " + add_acc.Split(' ')[1] +
                            "\n" + "Command.Info::Affected: Null");
                    }
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                JoinToQueue(SMSGCMD.SMSG_EXCEPTION_ERROR.ToString());

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
            //---->
            _pClient._ClientIsKeepAlive = true;
        }

        //===========================================================================================
        //
        //      HANDLE MSG: SMSG_GET_ACCOUNT_PROFILE
        //
        private void smsg_get_account_profile(string id_acc)
        {
            try
            {
                if (_pClient._ClientIsLogged)
                {
                    if(id_acc.Contains(" "))
                    {
                        string temp = string.Empty;
                        if (_pClient._pEntryPoint[2] == "5")
                        {
                            temp = _pClient.pSQLHandle.SQLReader(
                                  "SELECT a.id_account, a.id_level, a.linked_to, a.name, a.surname, a.tel_number, a.address, a.role, a.data" +
                                  " FROM accounts a WHERE a.id_account IN(" + id_acc.Split(' ')[1] + ") AND a.id_level < " +
                                  _pClient._pEntryPoint[2] + " ORDER BY a.id_account;");
                        }
                        else
                        {
                            temp = _pClient.pSQLHandle.SQLReader(
                                  "SELECT a.user, a.name, a.surname, a.tel_number, a.address" +
                                  " FROM accounts a WHERE a.id_account IN(" + id_acc.Split(' ')[1] + ") AND a.id_level < " +
                                  _pClient._pEntryPoint[2] + ";");
                        }

                        if (temp != "CX:0x01SQLERR")
                        {
                            JoinToQueue(temp + ((_pClient._pEntryPoint[2] == "5") ? "[FINAL]" : ""));
                            _pEvents.EventWriteInformation("Comando: SMSG_GET_ACCOUNT_PROFILE" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + temp.Length +
                            "\n" + "Command.Info::Text: " + temp);
                        }
                        else
                        {
                            JoinToQueue(SMSGCMD.SMSG_GET_ACCOUNT_PROFILE_ERR.ToString() + "[FINAL]");
                            _pEvents.EventWriteFailureAudit("Comando: SMSG_GET_ACCOUNT_PROFILE" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + SMSGCMD.SMSG_GET_ACCOUNT_PROFILE_ERR.ToString().Length +
                            "\n" + "Command.Info::Error: " + SMSGCMD.SMSG_GET_ACCOUNT_PROFILE_ERR.ToString() +
                            "\n" + "Command.Info::Text: " + id_acc);
                        }
                    }
                    else
                    {
                        JoinToQueue(SMSGCMD.SMSG_GET_ACCOUNT_PROFILE_ERR.ToString() + "[FINAL]");
                        _pEvents.EventWriteFailureAudit("Comando: SMSG_GET_ACCOUNT_PROFILE" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + SMSGCMD.SMSG_GET_ACCOUNT_PROFILE_ERR.ToString().Length +
                            "\n" + "Command.Info::Text: " + SMSGCMD.SMSG_GET_ACCOUNT_PROFILE_ERR.ToString() +
                            "\n" + "Command.Info::Text: " + id_acc);
                    }
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                JoinToQueue(SMSGCMD.SMSG_EXCEPTION_ERROR.ToString());

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
            //---->
            _pClient._ClientIsKeepAlive = true;
        }

        //===========================================================================================
        //
        //      HANDLE MSG: SMSG_KICK_ACCOUNT
        //
        private void smsg_account_kick(string id_kick)
        {
            try
            {
                if (_pClient._ClientIsLogged)
                {
                    if (id_kick.Contains(" "))
                    {
                        if (_pClient.pSQLHandle.SQLGetAffected(
                              "UPDATE accounts a SET a.online = 0 WHERE a.id_account IN(" + id_kick.Split(' ')[1] + ") AND a.id_level < " +
                                   _pClient._pEntryPoint[2] + ";", true) > 0)
                        {
                            JoinToQueue(SMSGCMD.SMSG_KICK_ACCOUNT_OK.ToString() + "[FINAL]");
                            _pEvents.EventWriteInformation("Comando: SMSG_KICK_ACCOUNT" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::KickID(s): " + id_kick.Split(' ')[1]);
                        }
                        else
                        {
                            JoinToQueue(SMSGCMD.SMSG_KICK_ACCOUNT_ERR.ToString() + "[FINAL]");
                            _pEvents.EventWriteFailureAudit("Comando: SMSG_KICK_ACCOUNT" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + SMSGCMD.SMSG_KICK_ACCOUNT_ERR.ToString().Length +
                            "\n" + "Command.Info::Text: " + SMSGCMD.SMSG_KICK_ACCOUNT_ERR.ToString());
                        }
                    }
                    else
                    {
                        JoinToQueue(SMSGCMD.SMSG_KICK_ACCOUNT_ERR.ToString() + "[FINAL]");
                        _pEvents.EventWriteFailureAudit("Comando: SMSG_KICK_ACCOUNT" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + SMSGCMD.SMSG_KICK_ACCOUNT_ERR.ToString().Length +
                            "\n" + "Command.Info::Text: " + SMSGCMD.SMSG_KICK_ACCOUNT_ERR.ToString());
                    }
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                JoinToQueue(SMSGCMD.SMSG_EXCEPTION_ERROR.ToString());

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
            //---->
            _pClient._ClientIsKeepAlive = true;
        }

        //===========================================================================================
        //
        //      HANDLE MSG: SMSG_HISTORY_ACCOUNT
        //
        private void smsg_account_history(string id_acc)
        {
            try
            {
                if (_pClient._ClientIsLogged)
                {
                    if (id_acc.Contains(" "))
                    {
                        string temp = _pClient.pSQLHandle.SQLReader(
                              "SELECT * FROM connections a WHERE a.id_account IN(" + id_acc + ") AND a.id_level < " +
                              _pClient._pEntryPoint[2] + " ORDER BY a.id_account;");

                        if (temp != "CX:0x01SQLERR")
                        {
                            JoinToQueue(temp + ((_pClient._pEntryPoint[2] == "5") ? "[FINAL]" : ""));
                            _pEvents.EventWriteInformation("Comando: SMSG_HISTORY_ACCOUNT" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + temp.Length +
                            "\n" + "Command.Info::Text: " + temp);
                        }
                        else
                        {
                            JoinToQueue(SMSGCMD.SMSG_HISTORY_ACCOUNT_ERR.ToString() + "[FINAL]");
                            _pEvents.EventWriteFailureAudit("Comando: SMSG_HISTORY_ACCOUNT" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + SMSGCMD.SMSG_HISTORY_ACCOUNT_ERR.ToString().Length +
                            "\n" + "Command.Info::Text: " + SMSGCMD.SMSG_HISTORY_ACCOUNT_ERR.ToString());
                        }
                    }
                    else
                    {
                        JoinToQueue(SMSGCMD.SMSG_HISTORY_ACCOUNT_ERR.ToString() + "[FINAL]");
                        _pEvents.EventWriteFailureAudit("Comando: SMSG_HISTORY_ACCOUNT" + "\n" +
                            "Remote.Data: Entry( " + _pClient._pEntryPoint[0] + " ), Username( " + _pClient._pEntryPoint[1] +
                            " ), Level( " + _pClient._pEntryPoint[2] + " ) " +
                            "\n" + "Command.Info::Len: " + SMSGCMD.SMSG_HISTORY_ACCOUNT_ERR.ToString().Length +
                            "\n" + "Command.Info::Text: " + SMSGCMD.SMSG_HISTORY_ACCOUNT_ERR.ToString());
                    }
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                JoinToQueue(SMSGCMD.SMSG_EXCEPTION_ERROR.ToString());

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
            //---->
            _pClient._ClientIsKeepAlive = true;
        }
    }
}
