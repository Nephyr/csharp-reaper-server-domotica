﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public class SQL
    {
        MySql.Data.MySqlClient.MySqlConnection Conn;
        MySql.Data.MySqlClient.MySqlCommand Cmd;
        MySql.Data.MySqlClient.MySqlDataReader Reader;

        //===========================================================================================

        public System.Diagnostics.EventLog pHandleLog;

        //===========================================================================================

        ~SQL()
        {
            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        public SQL(string address, string db, string user, string pass)
        {
            try
            {
                pHandleLog = new System.Diagnostics.EventLog();
                if (!System.Diagnostics.EventLog.SourceExists("Reaper Runtime Events"))
                {
                    System.Diagnostics.EventLog.CreateEventSource("Reaper Runtime Events", "Reaper Runtime Events");
                }
                pHandleLog.Source = "Reaper Runtime Events";
                pHandleLog.EnableRaisingEvents = true;
                //---->
                Conn = new MySql.Data.MySqlClient.MySqlConnection("SERVER=" + address + "; DATABASE=" + db + "; UID=" + user + "; PASSWORD=" + pass + ";");
                Cmd = Conn.CreateCommand();
                Cmd.CommandTimeout = 1000;
            }
            catch (MySql.Data.MySqlClient.MySqlException e)
            {
                pHandleLog.WriteEntry(e.ToString(), System.Diagnostics.EventLogEntryType.Error);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        public string SQLReader(string cmdquery)
        {
            Cmd.CommandText = string.Empty;
            string query_result = string.Empty;
            int rowsAffected = 0;
            //---->
            try
            {
                Cmd.CommandText = cmdquery;
                //---->
                Conn.Open();
                Reader = Cmd.ExecuteReader();
                //---->
                while (Reader.Read())
                {
                    // FIX partizione carico in Multi-Threading
                    System.Threading.Thread.Sleep(1);

                    if (rowsAffected > 0) query_result += ",";
                    rowsAffected++;
                    //---->
                    for (int i = 0; i < Reader.FieldCount; i++)
                    {
                        if (i == 0) query_result += ((Reader.GetValue(i).ToString() != "") ? Reader.GetValue(i).ToString() : "Null");
                        else query_result += "," + ((Reader.GetValue(i).ToString() != "") ? Reader.GetValue(i).ToString() : "Null");
                    }
                }
                //---->
                if (query_result.Trim() == string.Empty)
                    query_result = rowsAffected.ToString();
            }
            catch (MySql.Data.MySqlClient.MySqlException e)
            {
                pHandleLog.WriteEntry(e.ToString(), System.Diagnostics.EventLogEntryType.Error);
            }
            //---->
            Conn.Close();

            return query_result;
        }

        //===========================================================================================
        //
        //  QUERY DI LETTURA PER LOGIN
        //
        public int SQLGetAffected(string cmdquery, bool isAffected)
        {
            int rowsAffected = 0;
            //---->
            try
            {
                Cmd.CommandText = cmdquery;
                Conn.Open();
                //---->
                Reader = Cmd.ExecuteReader();
                if (!isAffected)
                {
                    while (Reader.Read())
                    {
                        // FIX partizione carico in Multi-Threading
                        System.Threading.Thread.Sleep(1);

                        rowsAffected++;
                    }
                }
                else
                {
                    rowsAffected = Reader.RecordsAffected;
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException e)
            {
                pHandleLog.WriteEntry(e.ToString(), System.Diagnostics.EventLogEntryType.Error);
            }
            //---->
            Conn.Close();

            return rowsAffected;
        }
    }
}
