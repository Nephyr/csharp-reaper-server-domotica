﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public class ReaperHttp : CSHTTPServer
    {
        public string Folder;

        //===========================================================================================

        public ReaperHttp(int thePort, string conf, string theFolder)
            : base(thePort, conf)
        {
            try
            {
                this.Folder = theFolder;
            }
            catch (System.Exception e)
            {
                _pEvent.EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        public override void OnResponse(ref HTTPRequestStruct rq, ref HTTPResponseStruct rp, string PHPParam)
        {
            try
            {
                string path = System.IO.Path.GetFullPath(this.Folder) + rq.URL.Replace("/", @"\");

                if (System.IO.Directory.Exists(path))
                {
                    if (System.IO.File.Exists(path + "index.html")) path += "index.html";
                    else if (System.IO.File.Exists(path + "index.htm")) path += "index.htm";
                    else if (System.IO.File.Exists(path + "index.php")) path += "index.php";
                    else
                    {
                        string[] dirs = System.IO.Directory.GetDirectories(path);
                        string[] files = System.IO.Directory.GetFiles(path);

                        string bodyStr = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"" + 
                            "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\>\"" + 
                            "<html xmlns=\"http://www.w3.org/1999/xhtml\\\"><head>" + 
                            "<title>Web Reaper - Index Of/</title>" + 
                            "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head>\n";
                        bodyStr += "<body><br /><b>Documenti disponibili\n</b><br />\n<ul>";
                        for (int i = 0; i < dirs.Length; i++)
                            bodyStr += "<li><a href = '" + rq.URL + System.IO.Path.GetFileName(dirs[i]) + "/'>[" +
                                System.IO.Path.GetFileName(dirs[i]) + "]</a></li>\n";
                        for (int i = 0; i < files.Length; i++)
                            bodyStr += "<li><a href = '" + rq.URL + System.IO.Path.GetFileName(files[i]) + "/'>" +
                                System.IO.Path.GetFileName(files[i]) + "</a></li>\n";
                        bodyStr += "</ul></body></html>\n";

                        rp.BodyData = System.Text.Encoding.ASCII.GetBytes(bodyStr);
                        return;
                    }
                }

                if (System.IO.File.Exists(path))
                {
                    HTTPMime _MimeType = new HTTPMime();
                    string s = _MimeType.MimeType(System.IO.Path.GetExtension(path));
                    byte[] sRes;
                    string temp_cache = string.Empty;
                    string line = string.Empty;
                    
                    /*System.IO.StreamReader sr = new System.IO.StreamReader(path);
                    while ((line = sr.ReadLine()) != null)
                    {
                        sRes += line + "\n";
                    }*/

                    using (System.IO.FileStream fs = System.IO.File.Open(path, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read))
                    {
                        byte[] b = new byte[fs.Length];
                        System.Text.UTF8Encoding temp = new System.Text.UTF8Encoding(true);
                        fs.Read(b, 0, (int)fs.Length);
                        sRes = b;
                    }

                    if (System.IO.Path.GetExtension(path) == ".php")
                    {
                        temp_cache = "file_" + System.Environment.TickCount + "_" + System.IO.Path.GetFileNameWithoutExtension(path) + ".cache";
                        System.IO.StreamWriter sw = new System.IO.StreamWriter(System.IO.Path.GetFullPath(this.Folder) + @"\" + temp_cache);
                        
                        sw.Write(PHPParam + System.Text.Encoding.ASCII.GetString(sRes));
                        sw.Close();

                        System.Diagnostics.ProcessStartInfo oInfo = new System.Diagnostics.ProcessStartInfo('"' + PHPRoot + '"' + ' ' + '"' +
                            System.IO.Path.GetFullPath(this.Folder) + @"\" + temp_cache + '"');
                        
                        oInfo.UseShellExecute = false;
                        oInfo.RedirectStandardOutput = true;
                        
                        System.Diagnostics.Process _p = System.Diagnostics.Process.Start(oInfo);
                        System.IO.StreamReader oReader2 = _p.StandardOutput;
                        sRes = System.Text.Encoding.ASCII.GetBytes(oReader2.ReadToEnd());
                        oReader2.Close();

                        System.IO.File.Delete(System.IO.Path.GetFullPath(this.Folder) + @"\" + temp_cache);
                    }

                    rp.fs = sRes;
                    if (s != "") rp.Headers["Content-type"] = s;
                }
                else
                {
                    rp.status = (int)RespState.NOT_FOUND;

                    string bodyStr = "<b style='font-family: arial;'>File richiesto non trovato!<br /><a href='javascript: history.go(-1);'>Torna Indietro.</a></b>";

                    rp.BodyData = System.Text.Encoding.ASCII.GetBytes(bodyStr);
                }
            }
            catch (System.Exception e)
            {
                _pEvent.EventWriteError(e);
                rp.BodyData = System.Text.Encoding.ASCII.GetBytes(" ");
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }
    }
}
