﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    enum RState
    {
        METHOD, URL, URLPARM, URLVALUE, VERSION,
        HEADERKEY, HEADERVALUE, BODY, OK
    }

    //===========================================================================================

    enum RespState
    {
        OK = 200,
        BAD_REQUEST = 400,
        NOT_FOUND = 404
    }

    //===========================================================================================

    public struct HTTPRequestStruct
    {
        public string Method;
        public string URL;
        public string Version;
        public System.Collections.Hashtable Args;
        public bool Execute;
        public System.Collections.Hashtable Headers;
        public int BodySize;
        public byte[] BodyData;
    }

    //===========================================================================================

    public struct HTTPResponseStruct
    {
        public int status;
        public string version;
        public System.Collections.Hashtable Headers;
        public int BodySize;
        public byte[] BodyData;
        public byte[] fs;
    }

    //===========================================================================================

    public class CsHTTPRequest
    {
        private System.Net.Sockets.TcpClient client;
        private RState ParserState;

        //===========================================================================================
        
        private HTTPRequestStruct HTTPRequest;
        private HTTPResponseStruct HTTPResponse;

        //===========================================================================================

        byte[] myReadBuffer;

        //===========================================================================================

        CSHTTPServer Parent;
        public EventLogger _pEvent = new EventLogger();

        //===========================================================================================

        public CsHTTPRequest(System.Net.Sockets.TcpClient client, CSHTTPServer Parent)
        {
            try
            {
                this.client = client;
                this.Parent = Parent;

                this.HTTPResponse.BodySize = 0;
            }
            catch(System.Exception e)
            {
                _pEvent.EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        public void Process()
        {
            System.Net.Sockets.NetworkStream ns = null;
            
            try
            {
                ns = client.GetStream();

                myReadBuffer = new byte[client.ReceiveBufferSize];
                System.String myCompleteMessage = string.Empty;
                int numberOfBytesRead = 0;

                string hValue = string.Empty;
                string hKey = string.Empty;
                int bfndx = 0;

                //----------------------------------------| PHP PARAM
                
                    string PHPParam = "";
                
                //----------------------------------------| PHP PARAM
                
                do
                {
                    // FIX partizione carico in Multi-Threading
                    System.Threading.Thread.Sleep(1);

                    numberOfBytesRead = ns.Read(myReadBuffer, 0, myReadBuffer.Length);
                    myCompleteMessage = System.String.Concat(myCompleteMessage, 
                                        System.Text.Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));

                    int ndx = 0;
                    do
                    {
                        // FIX partizione carico in Multi-Threading
                        System.Threading.Thread.Sleep(1);

                        switch (ParserState)
                        {
                            case RState.METHOD:
                                if (myReadBuffer[ndx] != ' ')
                                {
                                    HTTPRequest.Method += (char)myReadBuffer[ndx++];
                                    //----------------------------------------| PHP PARAM

                                        if (HTTPRequest.Method == "GET")
                                        {
                                            PHPParam = "$_GET";
                                        }
                                        else if (HTTPRequest.Method == "POST")
                                        {
                                            PHPParam = myCompleteMessage.Split('\n')[myCompleteMessage.Split('\n').Length - 1];
                                        }

                                    //----------------------------------------| PHP PARAM
                                }
                                else
                                {
                                    ndx++;
                                    ParserState = RState.URL;
                                    //----------------------------------------| PHP PARAM

                                        if (HTTPRequest.Method == "GET")
                                        {
                                            PHPParam += " = array('";
                                        }

                                    //----------------------------------------| PHP PARAM
                                }
                                break;

                            case RState.URL:
                                if (myReadBuffer[ndx] == '?')
                                {
                                    ndx++;
                                    hKey = "";
                                    HTTPRequest.Execute = true;
                                    HTTPRequest.Args = new System.Collections.Hashtable();
                                    ParserState = RState.URLPARM;
                                }
                                else if (myReadBuffer[ndx] != ' ')
                                    HTTPRequest.URL += (char)myReadBuffer[ndx++];
                                else
                                {
                                    ndx++;
                                    HTTPRequest.URL = System.Web.HttpUtility.UrlDecode(HTTPRequest.URL);
                                    ParserState = RState.VERSION;
                                }
                                break;

                            case RState.URLPARM:
                                if (myReadBuffer[ndx] == '=')
                                {
                                    ndx++;
                                    hValue = "";
                                    ParserState = RState.URLVALUE;
                                    //----------------------------------------| PHP PARAM

                                        if (HTTPRequest.Method == "GET")
                                        {
                                            PHPParam += "' => '";
                                        }

                                    //----------------------------------------| PHP PARAM
                                }
                                else if (myReadBuffer[ndx] == ' ')
                                {
                                    ndx++;

                                    HTTPRequest.URL = System.Web.HttpUtility.UrlDecode(HTTPRequest.URL);
                                    ParserState = RState.VERSION;
                                }
                                else
                                {
                                    hKey += (char)myReadBuffer[ndx];
                                    //----------------------------------------| PHP PARAM

                                        if (HTTPRequest.Method == "GET")
                                        {
                                            PHPParam += (char)myReadBuffer[ndx];
                                        }

                                    //----------------------------------------| PHP PARAM
                                    ndx++;
                                }
                                break;

                            case RState.URLVALUE:
                                if (myReadBuffer[ndx] == '&')
                                {
                                    ndx++;
                                    hKey = System.Web.HttpUtility.UrlDecode(hKey);
                                    hValue = System.Web.HttpUtility.UrlDecode(hValue);
                                    HTTPRequest.Args[hKey] = ((HTTPRequest.Args[hKey] != null) ? HTTPRequest.Args[hKey] + ", " + hValue : hValue);
                                    hKey = "";
                                    ParserState = RState.URLPARM;
                                    //----------------------------------------| PHP PARAM

                                        PHPParam += "', '";

                                    //----------------------------------------| PHP PARAM
                                }
                                else if (myReadBuffer[ndx] == ' ')
                                {
                                    ndx++;
                                    hKey = System.Web.HttpUtility.UrlDecode(hKey);
                                    hValue = System.Web.HttpUtility.UrlDecode(hValue);
                                    HTTPRequest.Args[hKey] = ((HTTPRequest.Args[hKey] != null) ? HTTPRequest.Args[hKey] + ", " + hValue : hValue);
                                    HTTPRequest.URL = System.Web.HttpUtility.UrlDecode(HTTPRequest.URL);
                                    ParserState = RState.VERSION;
                                }
                                else
                                {
                                    hValue += (char)myReadBuffer[ndx];
                                    //----------------------------------------| PHP PARAM

                                        if (HTTPRequest.Method == "GET")
                                        {
                                            if ((char)myReadBuffer[ndx] != '+')
                                            {
                                                PHPParam += (char)myReadBuffer[ndx];
                                            }
                                            else
                                            {
                                                PHPParam += " ";
                                            }
                                        }

                                    //----------------------------------------| PHP PARAM
                                    ndx++;
                                }
                                break;

                            case RState.VERSION:
                                if (myReadBuffer[ndx] == '\r') ndx++;
                                else if (myReadBuffer[ndx] != '\n') HTTPRequest.Version += (char)myReadBuffer[ndx++];
                                else
                                {
                                    ndx++;
                                    hKey = "";
                                    HTTPRequest.Headers = new System.Collections.Hashtable();
                                    ParserState = RState.HEADERKEY;
                                }
                                break;

                            case RState.HEADERKEY:
                                if (myReadBuffer[ndx] == '\r') ndx++;
                                else if (myReadBuffer[ndx] == '\n')
                                {
                                    ndx++;
                                    if (HTTPRequest.Headers["Content-Length"] != null)
                                    {
                                        HTTPRequest.BodySize = System.Convert.ToInt32(HTTPRequest.Headers["Content-Length"]);
                                        this.HTTPRequest.BodyData = new byte[this.HTTPRequest.BodySize];
                                        ParserState = RState.BODY;
                                    }
                                    else
                                        ParserState = RState.OK;

                                }
                                else if (myReadBuffer[ndx] == ':') ndx++;
                                else if (myReadBuffer[ndx] != ' ') hKey += (char)myReadBuffer[ndx++];
                                else
                                {
                                    ndx++;
                                    hValue = "";
                                    ParserState = RState.HEADERVALUE;
                                }
                                break;

                            case RState.HEADERVALUE:
                                if (myReadBuffer[ndx] == '\r') ndx++;
                                else if (myReadBuffer[ndx] != '\n') hValue += (char)myReadBuffer[ndx++];
                                else
                                {
                                    ndx++;
                                    HTTPRequest.Headers.Add(hKey, hValue);
                                    hKey = "";
                                    ParserState = RState.HEADERKEY;
                                }
                                break;

                            case RState.BODY:
                                System.Array.Copy(myReadBuffer, ndx, this.HTTPRequest.BodyData, bfndx, numberOfBytesRead - ndx);
                                bfndx += numberOfBytesRead - ndx;
                                ndx = numberOfBytesRead;
                                if (this.HTTPRequest.BodySize <= bfndx)
                                {
                                    ParserState = RState.OK;
                                }
                                break;
                        }
                    }
                    while (ndx < numberOfBytesRead);
                }
                while (ns.DataAvailable);

                //----------------------------------------| PHP PARAM

                    if (HTTPRequest.Method == "GET")
                    {
                        PHPParam += "');";
                    }
                    else
                    {
                        string[] _temp = PHPParam.Split('&');
                        int last = _temp.Length;
                        PHPParam = "$_POST = array(";

                        foreach (string _s in _temp)
                        {
                            last--;
                            PHPParam += "'" + _s.Replace("+", " ").Replace("=", "' => '") + "'";
                            if (last > 0) PHPParam += ",";
                        }
                        PHPParam += ");";
                    }

                //----------------------------------------| PHP PARAM

                HTTPResponse.version = "HTTP/1.1";

                if (ParserState != RState.OK) HTTPResponse.status = (int)RespState.BAD_REQUEST;
                else HTTPResponse.status = (int)RespState.OK;

                this.HTTPResponse.Headers = new System.Collections.Hashtable();
                this.HTTPResponse.Headers.Add("Server", Parent.Name);
                this.HTTPResponse.Headers.Add("Date", System.DateTime.Now.ToString("r"));

                //----------------------------------------| PHP PARAM
                    
                    PHPParam = "<?php " + PHPParam + " ?>";

                //----------------------------------------| PHP PARAM

                this.Parent.OnResponse(ref this.HTTPRequest, ref this.HTTPResponse, PHPParam);
                string HeadersString = this.HTTPResponse.version + " " + this.Parent.respStatus[this.HTTPResponse.status] + "\n";

                foreach (System.Collections.DictionaryEntry Header in this.HTTPResponse.Headers)
                {
                    HeadersString += Header.Key + ": " + Header.Value + "\n";
                }

                HeadersString += "\n";
                byte[] bHeadersString = System.Text.Encoding.ASCII.GetBytes(HeadersString);
                ns.Write(bHeadersString, 0, bHeadersString.Length);

                if (this.HTTPResponse.BodyData != null)
                {
                    ns.Write(this.HTTPResponse.BodyData, 0, this.HTTPResponse.BodyData.Length);
                }

                if (this.HTTPResponse.fs != null)
                {
                    ns.Write(this.HTTPResponse.fs, 0, this.HTTPResponse.fs.Length);
                }

                ns.Close();
                client.Close();
            }
            catch (System.Exception e)
            {
                ns.Close();
                client.Close();
                if (this.HTTPResponse.fs != null) this.HTTPResponse.fs = null;
                System.Threading.Thread.CurrentThread.Abort();
                //---->
                _pEvent.EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }
    }
}
