﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public abstract class CSHTTPServer
    {
        private int portNum                                     = 8080;
        private System.Net.Sockets.TcpListener listener         = null;

        System.Threading.Thread Thread                          = null;
        public System.Collections.Hashtable respStatus          = null;

        public string Name                                      = "Reaper HTTP - Server/2.1.0.100";

        public string PHPRoot                                   = null;

        public EventLogger _pEvent                              = new EventLogger();

        //===========================================================================================

        public bool IsAlive
        {
            get
            {
                return this.Thread.IsAlive;
            }
        }

        //===========================================================================================

        public CSHTTPServer()
        {
            try
            {
                respStatusInit();
            }
            catch (System.Exception e)
            {
                _pEvent.EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        public CSHTTPServer(int thePort, string conf)
        {
            try
            {
                PHPRoot = conf;
                portNum = thePort;
                respStatusInit();
            }
            catch (System.Exception e)
            {
                _pEvent.EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        private void respStatusInit()
        {
            try
            {
                respStatus = new System.Collections.Hashtable();

                respStatus.Add(200, "200 Ok");
                respStatus.Add(201, "201 Created");
                respStatus.Add(202, "202 Accepted");
                respStatus.Add(204, "204 No Content");

                respStatus.Add(301, "301 Moved Permanently");
                respStatus.Add(302, "302 Redirection");
                respStatus.Add(304, "304 Not Modified");

                respStatus.Add(400, "400 Bad Request");
                respStatus.Add(401, "401 Unauthorized");
                respStatus.Add(403, "403 Forbidden");
                respStatus.Add(404, "404 Not Found");

                respStatus.Add(500, "500 Internal Server Error");
                respStatus.Add(501, "501 Not Implemented");
                respStatus.Add(502, "502 Bad Gateway");
                respStatus.Add(503, "503 Service Unavailable");
            }
            catch (System.Exception e)
            {
                _pEvent.EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        public void Listen()
        {
            try
            {
                bool done = false;

                listener = new System.Net.Sockets.TcpListener(System.Net.IPAddress.Any, portNum);
                listener.Start();

                while (!done)
                {
                    // FIX partizione carico in Multi-Threading
                    System.Threading.Thread.Sleep(1);

                    if (listener.Pending())
                    {
                        CsHTTPRequest newRequest = new CsHTTPRequest(listener.AcceptTcpClient(), this);
                        System.Threading.Thread Thread = new System.Threading.Thread(new System.Threading.ThreadStart(newRequest.Process));
                        Thread.Name = "HTTP Request";
                        Thread.Start();
                    }
                }
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (System.Exception e)
            {
                _pEvent.EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        public void Start()
        {
            try
            {
                this.Thread = new System.Threading.Thread(new System.Threading.ThreadStart(this.Listen));
                this.Thread.Start();
            }
            catch (System.Exception e)
            {
                _pEvent.EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        public void Stop()
        {
            try
            {
                listener.Stop();
                this.Thread.Abort();
            }
            catch (System.Exception e)
            {
                _pEvent.EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        public abstract void OnResponse(ref HTTPRequestStruct rq, ref HTTPResponseStruct rp, string PHPParam);
    }
}
