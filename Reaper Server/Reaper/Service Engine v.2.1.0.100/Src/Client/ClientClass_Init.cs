﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectClient
    {
        public void StartSocketListener()
        {
            try
            {
                if (_pClient._pSocketClient != null)
                {
                    _pThreadDeamon = new System.Threading.Thread(new System.Threading.ThreadStart(_ClientDeamon));
                    _pThreadDeamon.Start();
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                _pClient._pSocketClient = null;

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
        }

        //===========================================================================================

        public void StopSocketListener()
        {
            try
            {
                if (_pClient._pSocketClient != null)
                {
                    _pClient._ClientIsRunning = false;

                    _pClientIsStopped = true;
                    _pClient._pSocketClient.Close();
                    _pClient._pSocketClient = null;

                    _pThreadDeamon.Join(1000);

                    // Se ancora attivo lo termino.
                    if (_pThreadDeamon.IsAlive)
                    {
                        _pThreadDeamon.Abort();
                    }
                    _pThreadDeamon = null;
                    _IsDeleted = true;
                }
            }
            catch(System.Threading.ThreadAbortException)
            {
                _pClient._pSocketClient = null;

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                return;
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                _pClient._pSocketClient = null;

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
        }

        //===========================================================================================

        public bool IsMarkedForDeletion()
        {
            return _IsDeleted;
        }

        public void MarkForDeletion()
        {
            _IsDeleted = true;
        }

        //===========================================================================================

        public void DropConnection()
        {
            try
            {
                if (_pClient._pSocketClient != null && _pClient._pSocketClient.Connected)
                {
                    _pClient._pSocketClient.Shutdown(System.Net.Sockets.SocketShutdown.Both);
                    _pClient._pSocketClient.Close();
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
        }
    }
}
