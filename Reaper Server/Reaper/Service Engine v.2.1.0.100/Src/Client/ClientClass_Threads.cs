﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectClient
    {
        private void _ClientDeamon()
        {
            try
            {
                System.Threading.Thread ReceiveQueueService             = null;
                System.Threading.Thread SendQueueService                = null;

                //===========================================================================================
                //  INIZIO CORPO - HANDLE CLIENT
                //===========================================================================================
                //
                //  CONTROLLO CLIENT
                //
                if (!IsClientCompatible())
                {
                    _pEvents.EventWriteWarning("Connessione rifiutata per l'Host( " + _pClient.RemoteEndPoint() + " )");

                    // Chiusura Socket + Thread e marcatura per cancellazione.
                    this.StopSocketListener();

                    return;
                }

                _pClient._pEntryPoint[0] = "";
                _pClient._pEntryPoint[1] = "";
                _pClient._pEntryPoint[2] = "";
                _pClient._pEntryPoint[3] = "";
                
                //
                //  AVVIO SERVIZIO DI QUEUING
                //
                ReceiveQueueService = new System.Threading.Thread(new System.Threading.ThreadStart(ReceiveQService));
                ReceiveQueueService.Start();

                SendQueueService = new System.Threading.Thread(new System.Threading.ThreadStart(SendQService));
                SendQueueService.Start();

                //
                //  ENABLE LOGIN
                //
                _pClient._AvailableCommands.Enqueue("SMSG_LOGIN_ACCOUNT");

                //===========================================================================================
                //
                //  LOOP (MAIN) HOST REMOTO
                //
                //===========================================================================================
                //
                //  APPLICATION HANDLE
                //  Gestore dei messaggi per applicazioni Reaper
                //
                while (_pClient._ClientIsRunning)
                {
                    // FIX partizione carico in Multi-Threading
                    System.Threading.Thread.Sleep(1);

                    if (_pClient._InQueue.Count > 0)
                    {
                        //
                        //  CONTROLLO RICHIESTA DISCONNESSIONE
                        //
                        if (_pClient._InQueue.Peek() == SMSGCMD.SMSG_SOCKET_CONN_FINISH.ToString() ||
                            _pClient._InQueue.Peek() == SMSGCMD.SMSG_SOCKET_CONN_ABORT.ToString() ||
                            _pClient._InQueue.Peek() == SMSGCMD.SMSG_SOCKET_CONN_ERR.ToString())
                        {
                            _pClient._ClientIsRunning = false;
                            //---->
                            _pClient._OutQueue.Clear();
                            _pClient._InQueue.Clear();
                            _pClient._AvailableCommands.Clear();
                        }

                        //
                        //  GESTIONE MESSAGGI CLIENT <--::--> SERVER
                        //
                        else if (IsPermitted(_pClient._InQueue.Peek().Split(' ')[0]))
                        {

                            if (_pClient._ClientIsLogged)
                            {
                                RegisterCommandToUser(_pClient._InQueue.Peek().Split(' ')[0]);
                            }

                            switch (_pClient._InQueue.Peek().Split(' ')[0])
                            {
                                //
                                //  GESTORE: COMMAND ---> Reaper.SMSGCMD.SMSG_LOGIN_ACCOUNT
                                //
                                case "SMSG_LOGIN_ACCOUNT":
                                    smsg_account_login();
                                    RetriveAllCommandsByLevel();
                                    break;

                                //
                                //  GESTORE: COMMAND ---> Reaper.SMSGCMD.SMSG_KEEP_ALIVE
                                //
                                case "SMSG_KEEP_ALIVE":
                                    JoinToQueue(SMSGCMD.SMSG_KEEP_ALIVE_OK.ToString());
                                    _pClient._ClientIsKeepAlive = true;
                                    break;

                                //
                                //  GESTORE: COMMAND ---> Reaper.SMSGCMD.SMSG_GET_ACCOUNT_LINKED
                                //
                                case "SMSG_GET_ACCOUNT_LINKED":
                                    if (_pClient._InQueue.Peek().Contains(" ") && _pClient._pEntryPoint[2] == "5")
                                    {
                                        smsg_account_linked_to(_pClient._InQueue.Peek().Split(' ')[1]);
                                    }
                                    else
                                    {
                                        smsg_account_linked_to("-1");
                                    }
                                    break;

                                //
                                //  GESTORE: COMMAND ---> Reaper.SMSGCMD.SMSG_ACCOUNT_ADD
                                //
                                case "SMSG_ACCOUNT_ADD":
                                    smsg_account_add(_pClient._InQueue.Peek());
                                    break;

                                //
                                //  GESTORE: COMMAND ---> Reaper.SMSGCMD.SMSG_ACCOUNT_ADD
                                //
                                case "SMSG_ACCOUNT_DEL":
                                    smsg_account_del(_pClient._InQueue.Peek().Split(' ')[1]);
                                    break;

                                //
                                //  GESTORE: COMMAND ---> Reaper.SMSGCMD.SMSG_SEARCH_ACCOUNT
                                //
                                case "SMSG_SEARCH_ACCOUNT":
                                    smsg_account_search_by(_pClient._InQueue.Peek());
                                    break;

                                //
                                //  GESTORE: COMMAND ---> Reaper.SMSGCMD.SMSG_HISTORY_ACCOUNT
                                //
                                case "SMSG_HISTORY_ACCOUNT":
                                    smsg_account_history(_pClient._InQueue.Peek());
                                    break;

                                //
                                //  GESTORE: COMMAND ---> Reaper.SMSGCMD.SMSG_KICK_ACCOUNT
                                //
                                case "SMSG_KICK_ACCOUNT":
                                    smsg_account_kick(_pClient._InQueue.Peek());
                                    break;

                                //
                                //  GESTORE: COMMAND ---> Reaper.SMSGCMD.SMSG_SESSION_DATA_REG_ADD
                                //
                                case "SMSG_SESSION_DATA_REG_ADD":
                                    smsg_session_data_reg_add(_pClient._InQueue.Peek());
                                    break;

                                //
                                //  GESTORE: COMMAND ---> Reaper.SMSGCMD.SMSG_GET_ACCOUNT_PROFILE
                                //
                                case "SMSG_GET_ACCOUNT_PROFILE":
                                    smsg_get_account_profile(_pClient._InQueue.Peek());
                                    break;

                                //
                                //  GESTORE: COMMAND ---> Reaper.SMSGCMD.SMSG_GET_ACCOUNT_PROFILE_UPDATE
                                //
                                case "SMSG_GET_ACCOUNT_PROFILE_UPDATE":
                                    smsg_get_account_profile_update(_pClient._InQueue.Peek());
                                    break;

                                //
                                //  GESTORE: COMMAND ---> Reaper.SMSGCMD.SMSG_SQL_GET_ALL_ON_USERS
                                //
                                case "SMSG_SQL_GET_ALL_ON_USERS":
                                    smsg_sql_get_all_on_users();
                                    break;

                                //
                                //  GESTORE: COMMAND_UNHANDLED
                                //
                                default: break;
                            }

                        }
                        else
                        {
                            JoinToQueue(SMSGCMD.SMSG_POLICY_LIV_ERROR.ToString());
                        }

                        //
                        //  RIMUOVO (ON TOP) IL COMANDO IN ESAME
                        //
                        if (_pClient._InQueue.Count > 0)
                            _pClient._InQueue.Dequeue();
                    }

                    //
                    //  KEEP-ALIVE JOIN COMMAND
                    //
                    if (_pClient._KeepAliveThread < System.Environment.TickCount)
                    {
                        if (!_pClient._ClientIsKeepAlive)
                        {
                            _pClient._ClientIsRunning = false;
                            JoinToQueue(SMSGCMD.SMSG_KEEP_ALIVE_ERR.ToString());
                        }
                        else
                        {
                            _pClient._ClientIsRunning = true;
                            _pClient._KeepAliveThread = System.Environment.TickCount + _pClient.GameWorldKeepAlive;
                            _pClient._ClientIsKeepAlive = false;
                        }
                    }
                }

                if (_pClient._pEntryPoint[0] != "Null" && _pClient._pEntryPoint[0] != null &&
                    _pClient._pEntryPoint[1] != "Null" && _pClient._pEntryPoint[1] != null &&
                    _pClient._pEntryPoint[2] != "Null" && _pClient._pEntryPoint[2] != null &&
                    _pClient._pEntryPoint[3] != "Null" && _pClient._pEntryPoint[3] != null)
                {
                    ClientLogout();

                    // Attesa pre-svuotamento Queuing
                    System.Threading.Thread.Sleep(1000);
                }

                _pClient._OutQueue.Clear();
                _pClient._InQueue.Clear();
                
                ReceiveQueueService.Join();
                SendQueueService.Join();

                //===========================================================================================
                //  FINE CORPO - HANDLE CLIENT
                //===========================================================================================
            }
            catch (System.Threading.ThreadAbortException)
            {
                // Chiusura Socket + Thread e marcatura per cancellazione.
                this.StopSocketListener();

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                return;
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

            // Chiusura Socket + Thread e marcatura per cancellazione.
            this.StopSocketListener();
        }
    }
}
