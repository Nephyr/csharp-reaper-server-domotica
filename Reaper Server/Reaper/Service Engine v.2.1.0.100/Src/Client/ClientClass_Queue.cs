﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectClient
    {
        //===========================================================================================
        //
        //      CORPO FUNZIONI / THREAD - COMMAND QUEUING
        //
        public void ReceiveQService()
        {
            byte[] bytesPending = System.BitConverter.GetBytes(0);
            string _resData = string.Empty;
            //---->
            try
            {
                while (_pClient._ClientIsRunning &&
                       _pClient._pSocketClient != null)
                {
                    // FIX partizione carico in Multi-Threading
                    System.Threading.Thread.Sleep(1);

                    // Check di possibili BIT pendenti
                    _pClient._pSocketClient.IOControl(System.Net.Sockets.IOControlCode.DataToRead, null, bytesPending);

                    // Se presenti BIT pendenti allora ricevo i dati altrimenti ciclo
                    if (System.BitConverter.ToUInt32(bytesPending, 0) > 0 && _pClient._ClientIsRunning)
                    {
                        _resData = ISyncReceive(_pClient._NetTimeout).ToString();
                        if (_resData != string.Empty)
                        {
                            _pClient._InQueue.Enqueue(_resData);
                        }
                    }
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
                _pClient._ClientIsRunning = false;

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                return;
            }
            catch (System.NullReferenceException)
            {
                _pClient._ClientIsRunning = false;

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                return;
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                _pClient._ClientIsRunning = false;
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        public void JoinToQueue(string AddToQueue)
        {
            try
            {
                _pClient._OutQueue.Enqueue(AddToQueue);
                return;
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                _pClient._ClientIsRunning = false;
            }
        }

        //===========================================================================================

        public void SendQService()
        {
            try
            {
                int offset = 0;
                while (_pClient._ClientIsRunning ||
                       offset > System.Environment.TickCount &&
                       _pClient._pSocketClient != null)
                {
                    // FIX partizione carico in Multi-Threading
                    System.Threading.Thread.Sleep(1);

                    // FIX AbortException: Prevengo la chiusura anticipata del Sender post-logout
                    if (!_pClient._ClientIsRunning && offset == 0)
                    {
                        offset = System.Environment.TickCount + 5000;
                    }

                    // Invio del primo messaggio, se presente, della coda
                    if (_pClient._OutQueue.Count > 0 && _pClient._ClientIsRunning)
                    {
                        SendPackage(_pClient._OutQueue.Dequeue(), _pClient._NetTimeout);
                    }
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
                _pClient._ClientIsRunning = false;

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                return;
            }
            catch (System.NullReferenceException)
            {
                _pClient._ClientIsRunning = false;

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                return;
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
                _pClient._ClientIsRunning = false;
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }
    }
}
