﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public class Client
    {
        public System.Net.Sockets.Socket _pSocketClient                     = null;

        public string[] _pEntryPoint                                        = new string[4]; 

        public System.Collections.Generic.Queue<string> _InQueue            = new System.Collections.Generic.Queue<string>();
        public System.Collections.Generic.Queue<string> _OutQueue           = new System.Collections.Generic.Queue<string>();
        
        public bool _ClientIsKeepAlive                                      = false;
        public bool _ClientIsLogged                                         = false;
        public bool _ClientIsRunning                                        = true;

        public SQL pSQLHandle                                               = null;
        public int _NetTimeout                                              = 5000;

        //===========================================================================================
        //
        //      DICHIARAZIONE STATICHE - Comandi
        //
        public System.Collections.Generic.Queue<string> _AvailableCommands  = new System.Collections.Generic.Queue<string>();

        //===========================================================================================
        //
        //  Inizializzo il KeepAlive (timer)
        //  Setto 5 Sec come finestra temporanea di login nel socket creato
        //

        public System.Int32 _KeepAliveThread                                = System.Environment.TickCount + 5000;
        public int GameWorldKeepAlive                                       = 0;

        //===========================================================================================

        public Client()
        {
        }

        ~Client()
        {
            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        public System.Net.EndPoint RemoteEndPoint()
        {
            try
            {
                if (_pSocketClient != null)
                {
                    return _pSocketClient.RemoteEndPoint;
                }
                return null;
            }
            catch (System.Exception)
            {
                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
                
                return null;
            }
        }
    }

    public partial class TCPObjectClient
    {
        private bool _pClientIsStopped                          = false;
        private System.Threading.Thread _pThreadDeamon          = null;
        private bool _IsDeleted                                 = false;

        //===========================================================================================

        public Client _pClient                                  = new Client();

        //===========================================================================================

        public EventLogger _pEvents                             = new EventLogger();

        //===========================================================================================

        // Costruttore
        public TCPObjectClient(System.Net.Sockets.Socket clientSocket, object pConfig)
        {
            try
            {
                _pClient._pSocketClient = clientSocket;
                _pClient.GameWorldKeepAlive = ((TCPObjectServer.GlobalNamespaceData)pConfig).GameWorldKeepAlive;
                _pClient.pSQLHandle = new SQL(((TCPObjectServer.GlobalNamespaceData)pConfig).MySQL_HostDatabase,
                                                                           ((TCPObjectServer.GlobalNamespaceData)pConfig).MySQL_DBTable,
                                                                           ((TCPObjectServer.GlobalNamespaceData)pConfig).MySQL_Username,
                                                                           ((TCPObjectServer.GlobalNamespaceData)pConfig).MySQL_Password);
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
        }

		// Distruttore
        ~TCPObjectClient()
        {
            try
            {
                StopSocketListener();

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
            }
        }
    }
}
