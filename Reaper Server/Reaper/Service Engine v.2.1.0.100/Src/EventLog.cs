﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public class EventLogger
    {
        private System.Diagnostics.EventLog pHandleLog = null;

        public EventLogger()
        {
            try
            {
                pHandleLog = new System.Diagnostics.EventLog();
                if (!System.Diagnostics.EventLog.SourceExists("Reaper Runtime Events"))
                {
                    System.Diagnostics.EventLog.CreateEventSource("Reaper Runtime Events", "Reaper Runtime Events");
                }
                pHandleLog.Source = "Reaper Runtime Events";
                pHandleLog.EnableRaisingEvents = true;
            }
            catch (System.Exception e)
            {
                EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        public void EventWriteInformation(string sMessage)
        {
            try
            {
                pHandleLog.WriteEntry(sMessage, System.Diagnostics.EventLogEntryType.Information);
            }
            catch (System.Exception e)
            {
                EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        public void EventWriteError(System.Exception sMessage)
        {
            try
            {
                pHandleLog.WriteEntry("===========================  EXCEPTION\n\n" + sMessage.ToString() +
                                           "\n\n===========================  STACK TRACE\n\n" + sMessage.StackTrace +
                                           "\n\n===========================  SOURCE\n\n" + sMessage.Source +
                                           "\n\n===========================  MESSAGE\n\n" + sMessage.Message +
                                           "\n\n===========================  INNER EXCEPTION\n\n" + sMessage.InnerException +
                                           "\n\n===========================  TARGET METHOD\n\n" + sMessage.TargetSite +
                                           "\n\n===========================  HELP LINK\n\n" + sMessage.HelpLink,
                                           System.Diagnostics.EventLogEntryType.Error);
            }
            catch (System.Exception e)
            {
                EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        public void EventWriteWarning(string sMessage)
        {
            try
            {
                pHandleLog.WriteEntry(sMessage, System.Diagnostics.EventLogEntryType.Warning);
            }
            catch (System.Exception e)
            {
                EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        public void EventWriteSuccessAudit(string sMessage)
        {
            try
            {
                pHandleLog.WriteEntry(sMessage, System.Diagnostics.EventLogEntryType.SuccessAudit);
            }
            catch (System.Exception e)
            {
                EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }

        //===========================================================================================

        public void EventWriteFailureAudit(string sMessage)
        {
            try
            {
                pHandleLog.WriteEntry(sMessage, System.Diagnostics.EventLogEntryType.FailureAudit);
            }
            catch (System.Exception e)
            {
                EventWriteError(e);
            }

            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }
    }
}
