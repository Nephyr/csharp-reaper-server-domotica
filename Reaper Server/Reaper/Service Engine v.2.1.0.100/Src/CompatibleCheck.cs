﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectClient
    {
        //===========================================================================================
        //
        //      ISYNC: PROTOCOLLO DI CONNESSIONE
        //      
        public bool IsClientCompatible()
        {
            try
            {
                if (System.Text.Encoding.ASCII.GetString(ReceivePackage(22, _pClient._NetTimeout)) ==
                    SMSGCMD.SMSG_SOCKET_CONN_START.ToString())
                {
                    SendPackage(SMSGCMD.SMSG_SOCKET_CONN_OK.ToString(), 500);
                    return true;
                }
                else
                {
                    SendPackage(SMSGCMD.SMSG_SOCKET_CONN_ERR.ToString(), 500);
                    return false;
                }
            }
            catch (System.Exception)
            {
                return false;
            }
        }
    }
}
