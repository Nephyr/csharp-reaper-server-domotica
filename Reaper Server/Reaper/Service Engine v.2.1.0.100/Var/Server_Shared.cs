﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectServer
    {

        //===========================================================================================

        public struct GlobalNamespaceData
        {
            //===========================================================================================
            //
            //      DICHIARAZIONE STATICHE - MySQL
            //
            public string                   MySQL_Username;
            public string                   MySQL_Password;
            public string                   MySQL_HostDatabase;
            public string                   MySQL_DBTable;
            public SQL                      pSQLHandle;

            //===========================================================================================
            //
            //      DICHIARAZIONE STATICHE - HTTP
            //
            public string                   PHPRoot;
            public string                   HttpDocumentRoot;
            public int                      HttpPort;

            //===========================================================================================
            //
            //      DICHIARAZIONE STATICHE - CICLO DI VITA
            //
            public int                      GameWorldPortServer;
            public System.Net.IPAddress     GameWorldBindIP;
            public int                      GameWorldKeepAlive;
        }

        public GlobalNamespaceData pConfig = new GlobalNamespaceData();
    }
}
