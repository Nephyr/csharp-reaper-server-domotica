﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectClient
    {
        //===========================================================================================
        //
        //      DEFINE - COMMAND ENUMERATOR
        //
        [System.Flags]
        public enum SMSGCMD
        {
            //
            //  SOCKET CONN
            //
            SMSG_SOCKET_CONN_OK = 10,                               // Conferma connessione
            SMSG_SOCKET_CONN_ERR,                                   // Errore connessione
            SMSG_SOCKET_CONN_START,                                 // Inizio connessione
            SMSG_SOCKET_CONN_FINISH,                                // Fine connessione
            SMSG_SOCKET_CONN_ABORT,                                 // Chiusura forzata/Kick connessione

            //
            //  KEEP-ALIVE (TTL)
            //
            SMSG_KEEP_ALIVE,                                        // Inizio KeepAlive
            SMSG_KEEP_ALIVE_OK,                                     // Connessione attiva
            SMSG_KEEP_ALIVE_ERR,                                    // Connessione persa 

            //
            //  MySQL QUERY
            //
            SMSG_SQL_QUERY_ERR,                                     // Errore nella query SQL ricevuta

            //
            //  LOGIN
            //
            SMSG_LOGIN_ACCOUNT,                                     // Richiesta login
            SMSG_LOGIN_ACCOUNT_OK,                                  // Login completato

            SMSG_LOGIN_ACCOUNT_ERR,                                 // Login fallito
            SMSG_LOGIN_ACCOUNT_REGCONN_ERR,                         // Login - Registrazione connessione fallita
            SMSG_LOGIN_ACCOUNT_UPDATE_ERR,                          // Login - Aggiornamento dati account fallito

            SMSG_LOGIN_ACC_MIRROR_ERR,                              // Tentativo di Login su account già settato --> Online

            //
            //  LOGOUT
            //
            SMSG_LOGOUT_ACCOUNT,                                    // Richiesta Logout
            SMSG_LOGOUT_ACCOUNT_OK,                                 // Logout completato
            SMSG_LOGOUT_ACCOUNT_ERR,                                // Logout fallito

            //
            //  REGISTRATION
            //
            SMSG_ACCOUNT_ADD,                                       // Richiesta registrazione account
            SMSG_ACCOUNT_ADD_OK,                                    // Richiesta OK
            SMSG_ACCOUNT_ADD_ERR,                                   // Errore nella richiesta

            //
            //  CANCELLATION
            //
            SMSG_ACCOUNT_DEL,                                       // Richiesta cancellazione account
            SMSG_ACCOUNT_DEL_OK,                                    // Richiesta OK
            SMSG_ACCOUNT_DEL_ERR,                                   // Errore nella richiesta

            //
            //  HISTORY
            //
            SMSG_HISTORY_ACCOUNT,                                   // Richiesta storico connessioni account
            SMSG_HISTORY_ACCOUNT_ERR,                               // Errore nella richiesta

            //
            //  SQL QUERY
            //
            SMSG_SQL_GET_ALL_ON_USERS,                              // Richiesta stampa (socket) users online

            //
            //  LINKED
            //
            SMSG_GET_ACCOUNT_LINKED,                                // Richiesta lista account associati a un ID
            SMSG_GET_ACCOUNT_LINKED_ERR,                            // Errore nella richiesta

            //
            //  SEARCH
            //
            SMSG_SEARCH_ACCOUNT,                                    // Richiesta lista account filtrati per ricerca
            SMSG_SEARCH_ACCOUNT_ERR,                                // Errore nella richiesta

            //
            //  PROFILE
            //
            SMSG_GET_ACCOUNT_PROFILE,                               // Richiesta profilo account per ID
            SMSG_GET_ACCOUNT_PROFILE_UPDATE,                        // Richiesta aggiornamento profilo account per ID
            SMSG_GET_ACCOUNT_PROFILE_UPDATE_OK,                     // Richiesta aggiornamento OK
            SMSG_GET_ACCOUNT_PROFILE_ERR,                           // Errore nella richiesta

            //
            //  SESSION DATA
            //
            SMSG_SESSION_DATA_REG_ADD,                              // Richiesta di registrazione di tipo "Bind to Account"
            SMSG_SESSION_DATA_REG_OK,                               // Richiesta completata OK
            SMSG_SESSION_DATA_REG_ERR,                              // Errore nell'esecuzione

            //
            //  SQL KICK
            //
            SMSG_KICK_ACCOUNT,                                      // Richiesta kick users online
            SMSG_KICK_ACCOUNT_OK,                                   // Richiesta kick users online OK
            SMSG_KICK_ACCOUNT_ERR,                                  // Richiesta kick users online ERR

            //
            //  POLICY ERROR
            //
            SMSG_POLICY_LIV_ERROR,                                  // Non si hanno i permessi necessari per l'esecuzione del comando

            //
            //  EXCEPTION ERROR
            //
            SMSG_EXCEPTION_ERROR                                    
        }
    }
}
