﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class TCPObjectServer
    {
        public void InitConfigService()
        {
            try
            {
                //===========================================================================================
                //
                //      INIZIALIZZAZIONE CONFIGURATION.INI
                //
                INIReader.INIReader MyConfig           = new INIReader.INIReader(
                                                              System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) +
                                                              "/reaper.ini");

                //===========================================================================================
                //
                //      INIZIALIZZAZIONE CONFIG HTTP
                //
                pConfig.HttpPort                        = System.Convert.ToInt32(MyConfig.ReadKey("HttpServer", "ServerPort"));
                pConfig.PHPRoot                         = MyConfig.ReadKey("HttpServer", "PHPRoot");
                pConfig.HttpDocumentRoot                = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\" +
                                                          MyConfig.ReadKey("HttpServer", "DocumentRoot");

                //===========================================================================================
                //
                //      INIZIALIZZAZIONE CONFIG DI RUNNING
                //
                pConfig.GameWorldPortServer             = System.Convert.ToInt32(MyConfig.ReadKey("Server", "ServerPort"));
                pConfig.GameWorldBindIP                 = System.Net.IPAddress.Parse(MyConfig.ReadKey("Server", "ServerBindIP"));
                pConfig.GameWorldKeepAlive              = System.Convert.ToInt32(MyConfig.ReadKey("Server", "ServerKeepAlive"));

                //===========================================================================================
                //
                //      DICHIARAZIONE STATICHE - MySQL
                //
                pConfig.MySQL_Username                 = MyConfig.ReadKey("MySQL", "Username");
                pConfig.MySQL_Password                 = MyConfig.ReadKey("MySQL", "Password");
                pConfig.MySQL_HostDatabase             = MyConfig.ReadKey("MySQL", "Hostdata");
                pConfig.MySQL_DBTable                  = MyConfig.ReadKey("MySQL", "Database");
                
                //===========================================================================================
                //
                //      CHECK E CONNESSIONE - MySQL
                //
                if(pConfig.MySQL_Username == "-Errore" ||
                   pConfig.MySQL_Password == "-Errore" || 
                   pConfig.MySQL_HostDatabase == "-Errore" || 
                   pConfig.MySQL_DBTable == "-Errore")
                {
                    _pEvents.EventWriteWarning("Servizio [Reaper Server] lettura informazioni di connessione per il DB Specificato." +
                    "\n" + "Reaper::MySQL.Data< " + pConfig.MySQL_DBTable + ", " + pConfig.MySQL_HostDatabase + ", " +
                    pConfig.MySQL_Username + ", LEN( pwd ): " + pConfig.MySQL_Password.Length + " >");
                }
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
        }
    }
}
