﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

namespace Reaper
{
    public partial class Reaper : System.ServiceProcess.ServiceBase
    {
        public EventLogger _pEvents                 = new EventLogger();
        public TCPObjectServer _pDeamon             = new TCPObjectServer();

        public CSHTTPServer _pDeamonHttp            = null;

        //===========================================================================================

        public Reaper()
        {
            try
            {
                InitializeComponent();
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
            }
        }

        //===========================================================================================

        protected override void OnStart(string[] args)
        {
            try
            {
                // Setto la priorità di tutto il processo.
                System.Diagnostics.Process.GetCurrentProcess().PriorityClass = System.Diagnostics.ProcessPriorityClass.High;

                // Avvio il demone del server
                _pDeamon.StartServer();

                // Avvio il demone del server http
                _pDeamonHttp = new ReaperHttp(_pDeamon.pConfig.HttpPort, _pDeamon.pConfig.PHPRoot, _pDeamon.pConfig.HttpDocumentRoot);
                _pDeamonHttp.Start();

                // Refresh delle risorse.
                System.Diagnostics.Process.GetCurrentProcess().Refresh();
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
            }
        }

        //===========================================================================================

        protected override void OnStop()
        {
            try
            {
                // Arresto il demone.
                _pDeamon.StopServer();

                // Arresto il demone http
                _pDeamonHttp.Stop();

                // Rilascio tutta la memoria.
                this.Dispose(true);
            }
            catch (System.Exception e)
            {
                _pEvents.EventWriteError(e);
            }
        }
    }
}
