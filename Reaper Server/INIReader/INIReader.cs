﻿//-------------------------------------------------------------//
//
//  Copyright (C) 2010 Istituto Lagrange, Università Bicocca  
//
//  Linguaggio:     Visual C-Sharp 2008 .NET
//  Database:       MySQL 5.x.x
//  Network:        TCP/IP
//
//-------------------------------------------------------------//
//
//  Produttori:     Vanzo Luca Samuele (Server)
//                  Cerizza Davide (Client)
//
//-------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections;

namespace INIReader
{
    public class INIReader
    {
        [DllImport("kernel32")]
        private static extern bool WritePrivateProfileString(string Section,string Key, string ValueToWrite, string FilePath);
        
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string Section,string Key, string DefaultKey,
            StringBuilder BufferToWrite,int SizeOfBuffer, string FilePath);
        
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileSection(string Section, byte[] BufferToWrite, int SizeOfBuffer, string FilePath);
        
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileSectionNames(byte[] BufferToWrite, int SizeOfBuffer, string FilePath);

        private string mPathFile;
        public System.Diagnostics.EventLog pHandleLog;

        public INIReader(string PathFile)
        {
            try
            {
                pHandleLog = new System.Diagnostics.EventLog();
                if (!System.Diagnostics.EventLog.SourceExists("Reaper Runtime Events"))
                {
                    System.Diagnostics.EventLog.CreateEventSource("Reaper Runtime Events", "Reaper Runtime Events");
                }
                pHandleLog.Source = "Reaper Runtime Events";
                pHandleLog.EnableRaisingEvents = true;
                //---->
                if (!System.IO.File.Exists(PathFile))
                {
                    pHandleLog.WriteEntry("System.IO.File( " + PathFile + " )::ReadException --- >File Not Found!",
                        System.Diagnostics.EventLogEntryType.Error);
                }
                mPathFile = PathFile;

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
            catch (System.Exception)
            {
                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
        }

        ~INIReader()
        {
            // Garbage Collection
            System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
        }
      
        public string Path
        {
            get
            {
                return mPathFile;
            }
            set
            {
                mPathFile = value;
            }
        }
       
        public string ReadKey(string Section, string Key)
        {
            try
            {
                if (mPathFile != "")
                {
                    long sizeRead = 0;
                    int sizeBuffer = 1024;
                    bool Do = true;
                    StringBuilder buff;

                    do
                    {
                        buff = new StringBuilder(sizeBuffer);
                        sizeRead = GetPrivateProfileString(Section, Key, "", buff, sizeBuffer, mPathFile);
                        if (sizeRead == (sizeBuffer - 1) || sizeRead == (sizeBuffer - 2))
                        {
                            sizeBuffer *= 2;
                            Do = true;
                        }
                        else
                        {
                            Do = false;
                        }
                        if (sizeRead == 0) break;
                    } while (Do);

                    if (sizeRead == 0) return "-Errore";
                    else return buff.ToString();
                }
                else return "-Errore";
            }
            catch (System.Exception)
            {
                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                return "-Errore";
            }
        }
     
        public bool WriteFile(string Section, string Key, string Value)
        {
            try
            {
                if (mPathFile != "")
                    return WritePrivateProfileString(Section, Key, Value, mPathFile);
                else 
                    return false;
            }
            catch (System.Exception)
            {
                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                return false;
            }
        }

        public void DumpFile()
        {
            try
            {
                if (mPathFile != "")
                {
                    try
                    {
                        ArrayList mArray = ReadAllSection();
                        foreach (string Section in mArray)
                        {
                            ArrayList Key = ReadSection(Section);
                        }
                    }
                    catch
                    {
                        throw new System.Exception();
                    }
                }

                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
            catch (System.Exception)
            {
                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);
            }
        }

        public ArrayList ReadAllSection()
        {
            try
            {
                if (mPathFile != "")
                {
                    long sizeRead = 0;
                    int sizeBuffer = 32767;
                    try
                    {
                        byte[] buff = new byte[sizeBuffer];

                        sizeRead = GetPrivateProfileSectionNames(buff, sizeBuffer, mPathFile);

                        string[] bString = Encoding.ASCII.GetString(buff).Trim('\0').Split('\0');
                        if (sizeRead != 0) return new ArrayList(bString);
                        else return null;
                    }
                    catch
                    {
                        return null;
                    }
                }
                else return null;
            }
            catch (System.Exception)
            {
                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                return null;
            }
        }
       
        public ArrayList ReadSection(string Section)
        {
            try
            {
                if (mPathFile != "")
                {
                    long sizeRead = 0;
                    int sizeBuffer = 32767;
                    try
                    {
                        byte[] buff = new byte[sizeBuffer];

                        sizeRead = GetPrivateProfileSection(Section, buff, sizeBuffer, mPathFile);

                        string[] bString = Encoding.ASCII.GetString(buff).Trim('\0').Split('\0');
                        if (sizeRead != 0) return new ArrayList(bString);
                        else return null;
                    }
                    catch
                    {
                        return null;
                    }
                }
                else return null;
            }
            catch (System.Exception)
            {
                // Garbage Collection
                System.GC.Collect(System.GC.GetGeneration(this), System.GCCollectionMode.Forced);

                return null;
            }
        }
       
        public string GetName(string Key)
        {
            try
            {
                string[] Value = Key.Split('=');
                return Value[0];
            }
            catch (System.Exception)
            {
                return "-Errore";
            }
        }
    }
}
